import os
import time
import signal
from datetime import datetime

while True:
    load = os.getloadavg()[1]
    if load > 25:
        f = open('.cassandra.pid', 'r')
        pid = f.readline()
        os.kill (int(pid), signal.SIGKILL)
        log = open('log/seppuku.out', 'a')
        now = datetime.now()
        log.write( "%s Cassandra killed! Load was %f \n" %
                  (now.strftime("%Y-%m-%d %H:%M"),load))
        break
    else:
        time.sleep(60)



