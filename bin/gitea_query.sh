#!/bin/sh

curl -s -X GET "http://pc-tdq-git.cern.ch/gitea/api/v1/repos/oks/$1/pulls"| \
jq -crM '.[] | select (.state=="open") | {request: .title, author: .user.full_name, state: .state, comment: .body, assigne: .assignee.full_name, URL: .html_url}'
