#!/bin/sh

echo "Starting SA in $1"
unset TDAQ_SETUP_P1
export TDAQ_DB_PATH=$1:$TDAQ_DB_PATH
ShifterAssistant $1 &>logs/sa.out &
pid=$!
echo "SA started, pid $pid"
sleep 25
kill $pid
errors=`cat log/aal.err | grep ERROR | wc -l`
[ "$errors" != "0" ] && { echo "Found errors in running SA: "; cat log/aal.err; rm data/*; exit -1 ;}
cat log/aal.err
rm log/* data/*
exit 0
