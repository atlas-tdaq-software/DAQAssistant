import xml.etree.ElementTree as ET
from xml.etree.ElementTree import ElementTree, Element, SubElement, Comment, tostring

#make the output easier to follow for human readers
from xml.dom import minidom

import sys
import os
import re 

def prettify(elem):
    ##Return a pretty-printed XML string for the Element.
    rough_string = tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

########################################################################
#Python script useful to create 'config.data.xml' file from directives
#sys.argv[1] is the path to directives directory

#Set the path to the 'aal.schema.xml'
schema = 'daq/schema/aal.schema.xml'
########################################################################


oks = ElementTree()
top = Element('oks-data')
oks._setroot(top)

DTP = "<!-- oks-data version 2.0 -->\n <!DOCTYPE oks-data [  <!ELEMENT oks-data (info, (include)?, (comments)?, (obj)+)>  <!ELEMENT info EMPTY>  <!ATTLIST info      name CDATA #REQUIRED      type CDATA #REQUIRED      num-of-items CDATA #REQUIRED      oks-format CDATA #FIXED \"extended\"      oks-version CDATA #REQUIRED      created-by CDATA #REQUIRED      created-on CDATA #REQUIRED      creation-time CDATA #REQUIRED      last-modified-by CDATA #REQUIRED      last-modified-on CDATA #REQUIRED      last-modification-time CDATA #REQUIRED  >  <!ELEMENT include (file)+>  <!ELEMENT file EMPTY>  <!ATTLIST file      path CDATA #REQUIRED  >  <!ELEMENT comments (comment)+>  <!ELEMENT comment EMPTY>  <!ATTLIST comment      creation-time CDATA #REQUIRED      created-by CDATA #REQUIRED      created-on CDATA #REQUIRED      author CDATA #REQUIRED      text CDATA #REQUIRED  >  <!ELEMENT obj (attr | rel)*>  <!ATTLIST obj      class CDATA #REQUIRED      id CDATA #REQUIRED  >  <!ELEMENT attr (#PCDATA)*>  <!ATTLIST attr      name CDATA #REQUIRED      type (bool|s8|u8|s16|u16|s32|u32|s64|u64|float|double|date|time|string|uid|enum|class|-) \"-\"      num CDATA \"-1\"  >  <!ELEMENT rel (#PCDATA)*>  <!ATTLIST rel      name CDATA #REQUIRED      num CDATA \"-1\"  >]>\n"


info = SubElement(top, 'info', {'name':'', 'type':'', 'num-of-items':'4','oks-format':'extended', 'oks-version':'oks-06-10-04 built &quot;Jul 11 2016&quot;', 'created-by':'gstagnit','created-on':'pc-tbed-pub-02.cern.ch', 'creation-time':'20160720T101408','last-modified-by':'gstagnit', 'last-modified-on':'paperino', 'last-modification-time':'20160721T094817'})

include = SubElement(top, 'include')
file_include = SubElement(include, 'file', {'path':schema})

config = SubElement(top, 'obj', {'class': 'SAConfig', 'id':'main'})
orderinit = SubElement(config, 'rel', {'name':'initialstatements'})
orderinit.text = ""
orderdir = SubElement(config, 'rel', {'name':'directives'})
orderdir.text = ""

initsets = {}
with open('InitialStatementsSetDescriptions.txt', 'r') as f:
        for line in f:
                splitLine = line.split()
                if len(splitLine) > 2:
                        initsets[splitLine[0]] = " ".join(splitLine[2:])

initsetscont = {}
with open('InitialStatementsSet.txt', 'r') as f:
        for line in f:
                splitLine = line.split()
                if len(splitLine) > 2:
                        initsetscont[splitLine[0]] = splitLine[2:]

count_init = 0
count_dir = 0
count_file = 0
for i in os.listdir(sys.argv[1]):
	for j in os.listdir(sys.argv[1]+"/"+i):
		
		count_dir_part = 0

		path = sys.argv[1]+"/"+i+"/"+j
		shortpath = i+"/"+j

		tree = ET.parse(path)
		root = tree.getroot()

		file_include = SubElement(include, 'file', {'path':shortpath})
		
		dirsetobj = SubElement(top, 'obj', {'class':'SADirectiveSet','id':j})
		dirset = SubElement(dirsetobj, 'rel', {'name':'Contains'})		
		dirset.text = ""

		for init in root.findall(".//obj"):
			if init.get('class') == 'SAInitialStatement' and "initeplcode" not in init.get('id'):
				orderinit.text += "bohSAInitialStatementboh boh"+init.get('id')+"boh\n"
				count_init += 1
			if init.get('class') == 'SADirective':
			#	orderdir.text += "bohSADirectiveboh boh"+init.get('id')+"boh\n"		
				dirset.text += "bohSADirectiveboh boh"+init.get('id')+"boh\n"			
				count_dir +=1
				count_dir_part +=1

		dirset.set('num',str(count_dir_part))
		orderdir.text += "bohSADirectiveSetboh boh"+j+"boh\n" # adding DirectiveSet instead of Directives
		count_file += 1


orderinit.set('num',str(count_init))
#orderdir.set('num',str(count_dir))
orderdir.set('num',str(count_file))

if ( len(initsets.keys()) == len(initsetscont.keys()) ):
	for i in initsets.keys():
		initSet = SubElement(top, 'obj', {'class':'SAInitialStatementSet', 'id':i})
		comm = SubElement(initSet, 'attr', {'name':'description','type':'string'})
		comm.text = "boh"+initsets.get(i)+"boh"
		cont = SubElement(initSet, 'rel', {'name':'Contains', 'num':str(len(initsetscont.get(i)))})
		cont.text = ""
		for j in initsetscont.get(i):
			cont.text += "bohSAInitialStatementboh boh"+j+"boh\n"
			

out_file = open("config.data.xml","w")
out_file.write("<?xml version=\"1.0\" ?>\n"+DTP+prettify(top).strip("<?xml version=\"1.0\" ?>").replace("boh","\""))
out_file.close()

#oks.write('output.xml', encoding="ASCII") #xml_declaration=None, method='xml')









