<?xml version="1.0" encoding="ISO-8859-1"?>
<?xml-stylesheet type="text/xsl" href="alerts.xsl"?>
<cassandra domain="ShiftLeader">

	<!--
	   ATLAS not taking data => To be done: error with varying severity depending on beam mode
	-->

	<directive name="ATLAS-not-taking-data">
	  <epl>
	    context ATLASRunning
	    select count(*) as cnt, avg(totalFraction)*100 as perc
	    from BusyFraction.win:time(300 sec) having ((count(*) &gt; 30) and (avg(totalFraction) &gt; 0.99))
	    output first every 10 minutes
	  </epl>
	  <listener type="alert">
	    <domain>AAL.ShiftLeader</domain>
	    <severity> FATAL </severity>
	    <message>No data taken in the last 5 minutes!</message>
	    <action>Mark in the logbook which procedure is causing (or has caused) this hick-up. You should already have alerted experts by now.</action>
	    <details>true</details>
	    <writer type="db">
	      <format>XML</format>
	    </writer>
	  </listener>	  
	</directive>

	<!--
	Check that EventDisplay is running if ATLAS is running
	-->

	<directive name="EventDisplay-not-running">
		<epl>
		  context ATLASRunning
		  select state from PartitionState(partitionName = 'EventDisplays', state != 'RUNNING')
		  output first every 10 minutes
		</epl>
		<listener type="alert">
			<domain>AAL.ShiftLeader</domain>
			<severity> ERROR </severity>
			<message>The EventDisplays partition is not in the RUNNING state (whereas ATLAS is running...)!</message>
			<action>Start EventDisplay partition or call the Event Display expert-on-call</action>
			<details>true</details>
			<writer type="db">
				<format>XML</format>
			</writer>
		</listener>
	</directive>
	<!-- 
	   Once LHC is in FLAT TOP use the "Main" event display instead of the "Standby" one
	  -->  
	<directive name="activate-main-event-display">
		<epl>
		  context ATLASRunning
		  select BeamMode as BeamMode
		  from LHCBeamMode(BeamMode='FLAT TOP')
		  where LHC_IN_PHYSICS
		</epl>
		<listener type="alert">
			<domain>AAL.ShiftLeader</domain>
			<severity> INFORMATION </severity>
			<message>LHC has reached $BeamMode$. Collisions coming soon?</message>
			<action>Make sure you are projecting the "Main" event display, instead of the "Standby/Cosmics" one. If LHC is going for collisions, alert the Pixel run coordinator.</action>
			<details>true</details>
			<writer type="db">
				<format>XML</format>
			</writer>
		</listener>
	</directive>
	

	<!--
		This directive generate an alert every time a wrong state is detected
		for a partition in the list below. Wrong state means!=RUNNING. 
	        The ALERT is generated immediately after the
		information is updated in IS, no polling needed. When a bad case is
		detected, in case we want to generate more then a single alert at
		specific time interval, we can add a time pattern or use the output
		keyword with timer:something.

	  -->
	<directive name="OLC-partition-in-wrong-state">
		<epl>
		  context ATLASRunning
		  select partitionName, state from PartitionState(partitionName='OLC', state != 'RUNNING')
		  where BEAM_MODE in ('RAMP', 'FLAT TOP','ADJUST', 'SQUEEZE', 'STABLE BEAMS')
		  output first every 10 minutes
		</epl>
		<listener type="alert">
			<domain>AAL.ShiftLeader</domain>
			<severity> ERROR </severity>
			<message>The partition $partitionName$ is not in correct state: $state$ while ATLAS is running.</message>
			<action>Please call the Luminosity on-call expert (72299)</action>
			<details>true</details>
			<writer type="email">
				<recipient>atlas-beamConds-elog@cern.ch</recipient>
			</writer>
			<writer type="db">
				<format>XML</format>
			</writer>
		</listener>
	</directive>
	
	<!--
		Check that ATLAS is running when Stable Beams declared This rule will
		trigger in both cases: when SB changes to 1 and ATLAS is not running,
		and also when ATLAS changes state to non-RUNNING during stable beams
	-->
	<directive name="stable-beams-atlas-not-running">
		<epl>
		  context StableBeams
			select status as status,  BEAM_MODE as beam_mode
			from ISEvent(partitionName='initial', name='LHC.StableBeamsFlag').std:lastevent(),
			ATLASState.std:lastevent()
			where status!='RUNNING' and BEAM_MODE='STABLE BEAMS'
			output first every 15 minutes
		</epl>
		<listener type="alert">
			<domain>AAL.ShiftLeader</domain>
			<severity>FATAL</severity>
			<message>LHC declared StableBeams, but ATLAS is not RUNNING (state = $status$)</message>
			<action>Make sure the ATLAS run is started soon as possible! This situation impacts the ATLAS data taking efficiency.</action>
			<details>true</details>
			 <writer type="db">
				<format>XML</format>
			</writer>
		</listener>
	</directive>

	<!-- At LCH Injection ATLAS partition must be in Running
	-->
	<directive name="beams-injection-atlas-not-running">
		<epl>
			select BeamMode as BeamMode
			from LHCBeamMode(BeamMode = 'INJECTION PHYSICS BEAM') 
			where ATLAS_IS_RUNNING=false and LHC_IN_PHYSICS=true
		</epl>
		<listener type="alert">
			<domain>AAL.ShiftLeader</domain>
			<severity>WARNING</severity>
			<message>LHC is injecting for physics, but ATLAS is not in RUNNING state</message>
			<action>Ask the trigger shifter to check that the correct SMK is stored in the database and make sure all detectors are ready to go into combined data taking mode. Then tell the Run Control shifter to start the run.</action>
			<details>true</details>
			<writer type="db">
				<format>XML</format>
			</writer>
		</listener>
	</directive>


	<!-- Alert if StableBeams not followed by Warm Start in 5 minutes-->
	<directive name="stable-beams-atlas-not-ready">
		<epl>
		  context StableBeams
		  select * from pattern
		  [every ISEvent(partitionName='initial', name='LHC.StableBeamsFlag', attributes('value').integer = 1) ->
		  (timer:interval(300 sec) and not ISEvent(partitionName = 'ATLAS',
		  name='RunParams.Ready4Physics', attributes('ready4physics').boolean = true))
		  ]
		  where ATLAS_IS_RUNNING and BEAM_MODE='STABLE BEAMS'
		</epl>
		<listener type="alert">
		  <domain>AAL.ShiftLeader</domain>
		  <severity> ERROR </severity>
		  <message>ATLAS is not in Physics mode 5 minutes after Stable Beams was declared!</message>
		  <action>Check the DCS state of IBL/PIX: they may block the warm start procedure. If those are READY, call immediately the DAQ on call, since something must have gone wrong with the automatic procedure. </action>
		  <details>false</details>
		  <writer type="db">
		    <format>XML</format>
		  </writer>
		</listener>
	</directive>

	<configuration>
	  <init-stmt>create variable boolean ATLAS_DCS_IS_READY =false </init-stmt>
    	  <init-stmt>
	    on  ISEvent(partitionName = 'ATLAS', server='DDC', type='DdcIntInfo', name regexp 'DDC.*.partitionState')
	    set ATLAS_DCS_IS_READY = (not exists(select * from ISEvent(partitionName = 'ATLAS', server='DDC', type='DdcIntInfo', name regexp 'DDC.*.partitionState').std:unique(name) where attributes('value').integer !=1))
      	  </init-stmt>
	</configuration>

	<!-- Alert if Ready4Physics not followed by ALL READY in 3 minutes -->

<!--
	<directive name="stable-beams-flag">
		<epl>
		  select attributes('value').int as sbflag, ATLAS_DCS_IS_READY as dcs
		  from ISEvent(partitionName = 'initial', name = 'LHC.StableBeamsFlag')
		</epl>
		<listener type="alert">
			<domain>AAL.ShiftLeader</domain>
			<severity> INFORMATION </severity>
			<message>Stable beams flag is $sbflag$, DCS state is $dcs$</message>
			<action> </action>
			<details>true</details>
			<writer type="db">
				<format>XML</format>
			</writer>
		</listener>
	</directive>
	
	<directive name="dcs-partition-state">
		<epl>
		  context StableBeams
		  select attributes('value').int as dcsflag, name as name
		  from ISEvent(partitionName = 'ATLAS', server='DDC', name regexp '.*.partitionState').std:unique(name).win:length(2).std:firstunique(attributes('value').int)
		</epl>
		<listener type="alert">
			<domain>AAL.ShiftLeader</domain>
			<severity> INFORMATION </severity>
			<message>$name$ is $dcsflag$</message>
			<action> </action>
			<details>true</details>
			<writer type="db">
				<format>XML</format>
			</writer>
		</listener>
	</directive>
-->	
	<directive name="dcs-global-state">
		<epl>
		  context StableBeams
		  select *
		  from ISEvent(partitionName = 'ATLAS', server='DDC', type='DdcIntInfo', name regexp 'DDC.*.partitionState').std:unique(name).win:keepall()
		  where (not exists(select * from ISEvent(partitionName = 'ATLAS', server='DDC', type='DdcIntInfo', name regexp 'DDC.*.partitionState', attributes('value').integer !=1).std:unique(name))) and BEAM_MODE='STABLE BEAMS'
		</epl>
		<listener type="alert">
			<domain>AAL.COM</domain>
			<severity> INFORMATION </severity>
			<message>Global DCS flag is READY</message>
			<action> </action>
			<details>true</details>
			<writer type="db">
				<format>XML</format>
			</writer>
		</listener>
	</directive>
	
	<directive name="atlas-not-ready-in-physics">
		<epl>
		  context ATLASReady4Physics
		  select ATLAS_LUMIBLOCKS_IN_PHYSICS as lbs
		  from ISEvent(partitionName = 'ATLAS', name = 'RunParams.LumiBlock')
		  where ATLAS_DCS_IS_READY = false and ATLAS_LUMIBLOCKS_IN_PHYSICS = 5
		</epl>
		<listener type="alert">
			<domain>AAL.ShiftLeader</domain>
			<severity> WARNING </severity>
			<message>ATLAS is taking Physics data since $lbs$ lumi blocks but the detector is not yet fully READY from a DCS point of view.</message>
			<action>Make sure with the other shifters that all detectors are brought to READY a.s.a.p. </action>
			<details>true</details>
			<writer type="db">
				<format>XML</format>
			</writer>
		</listener>
	</directive>


	<!--
	Clock check rules:
	Conditions:
	- ATLAS is in RUNNING;
	- The beam mode is none of NOBEAM, SETUP, ABORT, RAMPDOWN, CYCLING, RECOVERY;
	- The clock is set to "BCref" (internal).

	The actions could be something like:
	- Inform the Shift Leader;
	- Change the clock to BC1 (external) using the "Select LHC clock" utility in the "Ctrl Advanced" tab of the DAQPanel (select "Provided by LHC" in the dialog appearing on the screen);
	- Remember that after a manual change the Expert System will not automatically change the clock until the next UNCONFIGURE;
        - Inform the DAQ on-call of the issue.

        NOTE:
        BeamMode is updated only when the mode changes, is not republished!
   -->
          <directive name="wrong-clock-while-running">
<!--
	if after 30 seconds of ATLAS -> RUNNING and BeamMode -> "GOOD", "BCref" (=internal) appears as Clock source, then there is a problem
-->
            <epl>
	      context ATLASRunning
              select bm.BeamMode as BeamMode, cl.attributes('BCmainSelection').string as Clock from pattern
		[ 	every  bm=LHCBeamMode()
			->
			timer:interval(30 sec) and cl=ISEvent(partitionName = 'initial', name='LHC.RF2TTCApp-DCS')]
		where  	cl.attributes('BCmainSelection').string = 'BCref' and 
			bm.BeamMode in ('FLAT TOP', 'SQUEEZE', 'ADJUST', 'STABLE BEAMS', 'UNSTABLE BEAMS')
		output first every 10 minute
	    </epl> 

		<listener type="alert">
			<domain>AAL.ShiftLeader</domain>
			<severity>FATAL</severity>
			<message>Wrong clock setting ($Clock$) detected while ATLAS is in RUNNING state!</message>
                        <action>Tell the Run Control shifter to change the clock to BC1 (external) using the 
"Select LHC clock" utility in the "Ctrl Advanced" tab of the DAQPanel (select "Provided by LHC" in the dialog appearing on the screen);
Remember that after a manual change the Expert System will not automatically change the clock until the next UNCONFIGURE;
Inform the DAQ on-call of the issue.</action>
			<details>true</details>
			<writer type="db">
				<format>XML</format>
			</writer>
		</listener>
	</directive>
 
        <directive name="clock-change-running-info">

            <epl>
	      context ATLASRunning
	      select bm.BeamMode as BeamMode, cl.attributes('BCmainSelection').string as Clock from pattern 
	      [every	(  bm=LHCBeamMode(BeamMode in ('FLAT TOP', 'SQUEEZE', 'ADJUST', 'STABLE BEAMS', 'UNSTABLE BEAMS'))
	      ->
	      cl=ISEvent(partitionName = 'initial', name='LHC.RF2TTCApp-DCS')
	      where timer:within(30 sec))]
	      output every 1 minute
	    </epl> 

	    <listener type="alert">
	      <domain>AAL.TDAQ.Expert</domain>
	      <severity>INFORMATION</severity>
	      <message>Clock change to $Clock$ detected within 30 seconds in RUNNING state after LHC changed to beam mode $BeamMode$</message>
              <action>Keep an eye</action>
	      <details>true</details>
	      <writer type="db">
		<format>XML</format>
	      </writer>
	    </listener>

	</directive>

	<!--
	FATAL condition in OLC partiton
	  -->
	<directive name="olc-lumi-block-difference">
		<epl>   
		  context ATLASRunning
		  select messageTxt as Details, applicationName as appName
		  from Message( partitionName = 'OLC', applicationName in ('is2is_LumiBlockDifference', 'is2dip_LumiPerBunch_ATLAS_PREFERRED'), severity='FATAL')
		  output first every 10 minutes
		</epl>
		<listener type="alert">
			<domain>AAL.ShiftLeader</domain>
			<severity>FATAL</severity>
			<message>Application $appName$ sent a fatal error in the OLC partition: $Details$</message>
			<action>Inform the DQ shifter and call the Luminosity on-call expert (72299)</action>
			<details>true</details>
			<writer type="db">
				<format>XML</format>
			</writer>
		</listener>
	</directive>

	<!--
	Important application crashed in OLC partiton
	  -->
	<directive name="lumi-block-difference-watchdog-died">
		<epl>   
		  context ATLASRunning
		  select messageTxt as Details
		  from Message( 	partitionName = 'OLC', messageID = 'rc::ApplicationSignaled',
					parameters('application') in ('is2is_LumiBlockDifference', 'is2dip_LumiPerBunch_ATLAS_PREFERRED'),
					parameters('signal') not in ('9', '15') )
		  output first every 10 minutes
		</epl>
		<listener type="alert">
			<domain>AAL.ShiftLeader</domain>
			<severity>FATAL</severity>
			<message>OLC2COOL watchdog application died</message>
			<action>Inform the DQ shifter and call the Luminosity on-call expert (72299)</action>
			<details>true</details>
			<writer type="db">
				<format>XML</format>
			</writer>
	                <writer type="email">
                                <severity>ERROR</severity>
				<recipient>atlas-beamConds-elog@cern.ch</recipient>
				<recipient>0041754119041@mail2sms.cern.ch</recipient>
				<recipient>0041754112382@mail2sms.cern.ch</recipient>
        	        </writer>
		</listener>
	</directive>

	<directive name="lumi-blocks-in-physics">
		<epl>
			context ATLASReady4Physics
			select ATLAS_LUMIBLOCKS_IN_PHYSICS as LBsPhys, ATLAS_LUMIBLOCK as LBs, ATLAS_LUMIBLOCKS_START_PHYSICS as LBsStartPhys
			from ISEvent(partitionName = 'ATLAS', name = 'RunParams.LumiBlock')
			output last every 30 minutes
		</epl>
		<listener type="alert">
			<domain>AAL.COM</domain>
			<severity>INFORMATION</severity>
			<message>Lumiblocks in Physics: $LBsPhys$</message>
			<action>Enjoy</action>
			<details>true</details>
			<writer type="db">
				<format>XML</format>
			</writer>
		</listener>
	</directive>

	<!-- SMS to Run Manager(s) in case ATLAS is in bad busy state in physics
		(161801)
	-->
	<directive name="ATLAS-busy-in-physics-5mins">
	  <epl>
	    context ATLASReady4Physics
	    select Math.round(avg(totalFraction)*100) as percBusy
	    from BusyFraction.win:time(5 minutes) 
	    having (count(*) &gt; 30) and (avg(totalFraction) &gt; 0.95)
	    output first every 15 minutes
	  </epl>
	  <listener type="alert">
	    <domain>AAL.ShiftLeader</domain>
	    <severity> FATAL </severity>
	    <message>Average ATLAS deadtime is $percBusy$ % in last 5 minutes.</message>
	    <action>Wake up and react promptly.</action>
	    <details>true</details>
	    <writer type="db">
	      <format>XML</format>
	    </writer>
	    <writer type="email">
            	<severity>FATAL</severity>
	        <recipient>Andrei.Kazarov@cern.ch</recipient>
		<recipient>0041754111801@mail2sms.cern.ch</recipient>
 	        <recipient>0041754115636@mail2sms.cern.ch</recipient>
	        <recipient>0041754111999@mail2sms.cern.ch</recipient>
      	    </writer>
	  </listener>	  
	</directive>

	<directive name="LHC-squeezing-physics">
	  <epl>
	    select *
	    from LHCBeamMode( BeamMode = 'SQUEEZE' )
	    where LHC_IN_PHYSICS
	    output first every 20 minutes
	  </epl>
	  <listener type="alert">
	    <domain>AAL.ShiftLeader</domain>
	    <severity>INFORMATION</severity>
	    <message>LHC is squeezing the beams in physics mode.</message>
	    <action>Collisions are imminent? Take care.</action>
	    <details>false</details>
            <writer type="email">
	      <severity>INFORMATION</severity>
	      <recipient>0041754111801@mail2sms.cern.ch</recipient>
	      <recipient>0041754115636@mail2sms.cern.ch</recipient>
	      <recipient>0041754111999@mail2sms.cern.ch</recipient>
	    </writer>
	  </listener>
	</directive>

	<directive name="trigger-left-on-hold-SL">
          <epl>
	    select recovery.name as name, recovery.type as type, recovery.component as component, busycount.int as bsycnt, recovery.detector as detector
	    from AtlasChipRecovery(status='FAILED').std:lastevent() as recovery,  method:daq.EsperUtils.ISReader.getInfoByName("ATLAS", "RunParams.GlobalBusy", "RunControlBusy") as busycount
	    where busycount.int &gt; 0
	    output first every 10 minutes
	</epl>
	<listener type="alert">
		<domain>AAL.ShiftLeader</domain>
		<severity>ERROR</severity>
		<message>The expert system failed completing automated recoveries $name$-$type$ on component $component$ (detector $detector$). The trigger was left on hold $bsycnt$ times.</message>
                <action>After the problem with subsystem is fixed, trigger needs to be resumed manually in IGUI.</action>
		<details>true</details>
                <writer type="db">
			<format>XML</format>
                </writer>
	</listener>
	</directive>

	<directive name="collisions-detected">
          <epl>
	    context ATLASRunning
	    select avg(attributes('CalibLumi').double) as InstLumiAv
	    from ISEvent(partitionName = 'OLC', type='OCLumi', name='OLC.OLCApp/BCM_PREFERRED_Inst/BCM_2012_H_EventORAC_Average_Inst').win:time_batch(10 seconds)
	    having avg(attributes('CalibLumi').double) &gt; 0.2 
	    output first every 2 days
	</epl>
	<listener type="alert">
		<domain>AAL.ShiftLeader</domain>
		<severity>WARNING</severity>
		<message>Collisions are detected by OLC in last 10 seconds</message>
                <action>Make sure the run settings are what we expect</action>
		<details>true</details>
                <writer type="db">
			<format>XML</format>
                </writer>
                <writer type="email">
                  <severity>WARNING</severity>
		  <recipient>Andrei.Kazarov@cern.ch</recipient>
                </writer>
	</listener>
	</directive>

	<!--
	Latency mismatch reported by a susbsystem.
	Van be of different severity
	FATAL	rc::LatencyMismatch 	TRTEndcapA_E-03 TDM delay + CTP delay = 20, should be 16! Call TRT DAQ on-call: 160531 
	detector: TRT latency issue. reason: TRT latency issue.
	  -->
	<directive name="latency-mismatch">
		<epl>   
		  select parameters('detector') as Detector, messageTxt as Details
		  from Message( partitionName = 'ATLAS', messageID = 'rc::LatencyMismatch')
		  group by parameters('detector')
		  output first every 10 minutes
		</epl>
		<listener type="alert">
			<domain>AAL.ShiftLeader</domain>
			<severity>FATAL</severity>
			<message>Detector $Detector$ is misconfigured: its latency does not match ATLAS latency</message>
			<action>Call relevant detector expert</action>
			<details>true</details>
			<writer type="db">
				<format>XML</format>
			</writer>
		</listener>
		<listener type="alert">
			<domain>AAL.TDAQ.RunControl</domain>
			<severity>FATAL</severity>
			<message>Detector $Detector$ is misconfigured: its latency does not match ATLAS latency</message>
			<action>Notify Shift Leader, call relevant detector expert</action>
			<details>true</details>
			<writer type="db">
				<format>XML</format>
			</writer>
		</listener>
	</directive>


</cassandra>
