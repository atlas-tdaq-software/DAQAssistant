import xml.etree.ElementTree as ET
from xml.etree.ElementTree import ElementTree, Element, SubElement, Comment, tostring

#make the output easier to follow for human readers
from xml.dom import minidom

import sys

from os.path import basename

import re 

def prettify(elem):
    ##Return a pretty-printed XML string for the Element.
    rough_string = tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")


##################################################################
#See 'conversion' executable

#Path to 'aal.schema.xml' to be included in any file
schema = 'daq/schema/aal.schema.xml'
##################################################################


commentsDict = {}
with open('DirectivesDescriptions.txt', 'r') as f:
	for line in f:
        	splitLine = line.split()
		if len(splitLine) > 2:
			commentsDict[splitLine[0]] = " ".join(splitLine[2:])
			
commentsDictInit = {}
with open('InitialStatementsDescriptions.txt', 'r') as f:
	for line in f:
        	splitLine = line.split()
		if len(splitLine) > 2:
			commentsDictInit[splitLine[0]] = " ".join(splitLine[2:])			
			
			
tree = ET.parse(sys.argv[1])
root = tree.getroot()

oks = ElementTree()
top = Element('oks-data')
oks._setroot(top)

DTP = "<!-- oks-data version 2.0 -->\n <!DOCTYPE oks-data [  <!ELEMENT oks-data (info, (include)?, (comments)?, (obj)+)>  <!ELEMENT info EMPTY>  <!ATTLIST info      name CDATA #REQUIRED      type CDATA #REQUIRED      num-of-items CDATA #REQUIRED      oks-format CDATA #FIXED \"extended\"      oks-version CDATA #REQUIRED      created-by CDATA #REQUIRED      created-on CDATA #REQUIRED      creation-time CDATA #REQUIRED      last-modified-by CDATA #REQUIRED      last-modified-on CDATA #REQUIRED      last-modification-time CDATA #REQUIRED  >  <!ELEMENT include (file)+>  <!ELEMENT file EMPTY>  <!ATTLIST file      path CDATA #REQUIRED  >  <!ELEMENT comments (comment)+>  <!ELEMENT comment EMPTY>  <!ATTLIST comment      creation-time CDATA #REQUIRED      created-by CDATA #REQUIRED      created-on CDATA #REQUIRED      author CDATA #REQUIRED      text CDATA #REQUIRED  >  <!ELEMENT obj (attr | rel)*>  <!ATTLIST obj      class CDATA #REQUIRED      id CDATA #REQUIRED  >  <!ELEMENT attr (#PCDATA)*>  <!ATTLIST attr      name CDATA #REQUIRED      type (bool|s8|u8|s16|u16|s32|u32|s64|u64|float|double|date|time|string|uid|enum|class|-) \"-\"      num CDATA \"-1\"  >  <!ELEMENT rel (#PCDATA)*>  <!ATTLIST rel      name CDATA #REQUIRED      num CDATA \"-1\"  >]>\n"


info = SubElement(top, 'info', {'name':'', 'type':'', 'num-of-items':'4','oks-format':'extended', 'oks-version':'oks-06-10-04 built &quot;Jul 11 2016&quot;', 'created-by':'gstagnit','created-on':'pc-tbed-pub-02.cern.ch', 'creation-time':'20160720T101408','last-modified-by':'gstagnit', 'last-modified-on':'paperino', 'last-modification-time':'20160721T094817'})

include = SubElement(top, 'include')
file_include = SubElement(include, 'file', {'path':schema})


#Setting initial statement

count_init = 0
if (root.find('configuration') != None):
	for conf in root.findall('configuration'):
		for initialstmt in conf.findall('init-stmt'):
		
			inst = SubElement(top, 'obj', {'class':'SAInitialStatement', 'id':'initstmt'+'-'+basename(sys.argv[1])+'-'+str(count_init)})
			code = SubElement(inst, 'attr', {'name':'code','type':'string'})
			code.text = "boh"+initialstmt.text.strip()+"boh"
			if 'initstmt'+'-'+basename(sys.argv[1])+'-'+str(count_init) in commentsDictInit:
				comment = SubElement(inst, 'attr', {'name':'description', 'type':'string'})
				comment.text = "boh"+commentsDictInit['initstmt'+'-'+basename(sys.argv[1])+'-'+str(count_init)]+"boh"

			count_init += 1

if (root.findall('init-stmt') != None):
	for initialstmt in root.findall('init-stmt'):
		
		inst = SubElement(top, 'obj', {'class':'SAInitialStatement', 'id':'initstmt'+'-'+basename(sys.argv[1])+'-'+str(count_init)})
		code = SubElement(inst, 'attr', {'name':'code','type':'string'})
		code.text = "boh"+initialstmt.text.strip()+"boh"
		if 'initstmt'+'-'+basename(sys.argv[1])+'-'+str(count_init) in commentsDictInit:
			comment = SubElement(inst, 'attr', {'name':'description', 'type':'string'})
			comment.text = "boh"+commentsDictInit['initstmt'+'-'+basename(sys.argv[1])+'-'+str(count_init)]+"boh"

		count_init += 1

#for directive in root.findall('directive'):
#	if (directive.find('init-stmt') != None):
#		for initialstmt in directive.findall('init-stmt'):
#		
#			inst = SubElement(top, 'obj', {'class':'InitialStatement', 'id':'initstmt'+'-'+basename(sys.argv[1])+'-'+str(count_init)})
#			code = SubElement(inst, 'attr', {'name':'code','type':'string'})
#			code.text = "boh"+initialstmt.text.strip()+"boh"
#			order = SubElement(inst, 'attr', {'name':'order','type':'u32'})
#			order.text = str(count_init)
#			source = SubElement(inst, 'attr', {'name':'source','type':'string'})
#			source.text = "boh"+re.sub('xml','data.xml', basename(sys.argv[1]))+"boh"
#			count_init += 1



#Setting directive

count_dir = 0
for directive in root.findall('directive'):

	diroks = SubElement(top, 'obj', {'class':'SADirective', 'id':directive.get('name')})
#	active = SubElement(diroks, 'attr', {'name':'active','type':'bool'})
#	active.text = "0"
#	name = SubElement(diroks, 'attr', {'name':'name','type':'string'})
#	name.text = "boh"+directive.get('name')+"boh"
	if directive.get('name') in commentsDict:
		comment = SubElement(diroks, 'attr', {'name':'description', 'type':'string'})
		comment.text = "boh"+commentsDict[directive.get('name')]+"boh"


	eplrel = SubElement(diroks, 'rel', {'name':'epl'})

	#Setting initstmt related to directive

	if (directive.find('init-stmt') != None):
		
		initeplrel = SubElement(diroks, 'rel', {'name':'initialstatements'}) 
		initeplrel.text = ""
		
		count_initdir = 0
		if len(directive.findall('init-stmt')) != 0:
			for initialstmt in directive.findall('init-stmt'):

				initdir = SubElement(top, 'obj', {'class':'SAInitialStatement', 'id':'initeplcode-'+directive.get('name')+'-'+str(count_initdir)})
				initcode = SubElement(initdir, 'attr', {'name':'code','type':'string'})
				initcode.text = "boh"+initialstmt.text.strip()+"boh"

				initeplrel.text += "bohSAInitialStatementboh bohiniteplcode-"+directive.get('name')+'-'+str(count_initdir)+"boh"
				count_initdir += 1

			initeplrel.set('num',str(count_initdir))

	#Setting EPL Statement

	eploks = SubElement(top, 'obj', {'class':'SADirectiveStatement', 'id':'eplcode-'+directive.get('name')})
	code = SubElement(eploks, 'attr', {'name':'code','type':'string'})
	code.text = "boh"+directive.find('epl').text.strip()+"boh"

	eplrel.text = "bohSADirectiveStatementboh boheplcode-"+directive.get('name')+"boh"

	#Setting Listeners
	
	count_list = 0
	listrel = SubElement(diroks, 'rel', {'name':'listeners', 'num':str(count_list)})
	listrel.text = ""	
	for listener in directive.findall('listener'):
		listoks = SubElement(top, 'obj', {'class':'SAListener', 'id':'list'+str(count_list)+'-'+directive.get('name')})
		severity = SubElement(listoks, 'attr', {'name':'severity','type':'string'})
		severity.text = "boh"+listener.find('severity').text+"boh"
		domain = SubElement(listoks, 'attr', {'name':'domain','type':'string'})
		domain.text = "boh"+listener.find('domain').text+"boh"
		message = SubElement(listoks, 'attr', {'name':'message','type':'string'})
		message.text = "boh"+listener.find('message').text+"boh"
		action = SubElement(listoks, 'attr', {'name':'action','type':'string'})
		action.text = "boh"+listener.find('action').text+"boh"
		eventDetails = SubElement(listoks, 'attr', {'name':'eventDetails','type':'bool'})
		if (listener.find('details').text == "true"):
			eventDetails.text = "0"
		if (listener.find('details').text == "false"):
			eventDetails.text = "1"

		writersrel = SubElement(listoks, 'rel', {'name':'writers', 'num':'0'})
		writersrel.text = ""

		wrt_count = 0
		for writer in listener.findall('writer'):

			out = writer.get('type')		
			
			if (out == 'file'):
			#	filerel = SubElement(listoks, 'rel', {'name':'file'})
			#	filerel.text = "bohFileoutboh bohfile"+str(count_list)+"-"+directive.get('name')+"boh"
				writersrel.text += "bohSAFileboh bohfile"+str(count_list)+"-"+directive.get('name')+"boh"
				wrt_count += 1
	
				fileoks = SubElement(top, 'obj', {'class':'SAFile', 'id':'file'+str(count_list)+'-'+directive.get('name')})
			
				if (writer.find('partition') != None):
					partition = SubElement(fileoks, 'attr', {'name':'partition','type':'string'})
					partition.text = "boh"+writer.find('partition').text+"boh"

				if (writer.find('severity') != None):
					severity = SubElement(fileoks, 'attr', {'name':'severity','type':'string'})
					severity.text = "boh"+writer.find('severity').text+"boh"

				if (writer.find('append') != None):
					append = SubElement(fileoks, 'attr', {'name':'append','type':'bool'})
					if (writer.find('append').text == "true"):
						append.text = "0"
					if (writer.find('append').text == "false"):
						append.text = "1"

			if (out == 'db'):
			#	dbrel = SubElement(listoks, 'rel', {'name':'db'})
			#	dbrel.text = "bohDbboh bohdb"+str(count_list)+"-"+directive.get('name')+"boh"
				writersrel.text += "bohSADbboh bohdb"+str(count_list)+"-"+directive.get('name')+"boh"
				wrt_count += 1

				dboks = SubElement(top, 'obj', {'class':'SADb', 'id':'db'+str(count_list)+'-'+directive.get('name')})	
				formato = SubElement(dboks, 'attr', {'name':'format','type':'string'})
				formato.text = "boh"+writer.find('format').text+"boh"


			if (out == 'email'):
			#	emailrel = SubElement(listoks, 'rel', {'name':'email'})
			#	emailrel.text = "bohEmailboh bohemail"+str(count_list)+"-"+directive.get('name')+"boh"
				writersrel.text += "bohSAEmailboh bohemail"+str(count_list)+"-"+directive.get('name')+"boh"
				wrt_count += 1

				emailoks = SubElement(top, 'obj', {'class':'SAEmail', 'id':'email'+str(count_list)+'-'+directive.get('name')})	
			
				rec_count = 0
				rec_list = []
				for rec in writer.findall('recipient'):
					rec_count += 1
					rec_list.append(rec.text)
				rec_str = 'boh boh'.join(rec_list)

				address = SubElement(emailoks, 'attr', {'name':'addresses','type':'string', 'num':str(rec_count)})
				address.text = "boh"+rec_str+"boh"

				if (writer.find('severity') != None):
					severityemail = SubElement(emailoks, 'attr', {'name':'severity','type':'string'})
					severityemail.text = "boh"+writer.find('severity').text+"boh"


			writersrel.set('num',str(wrt_count))
		
		listrel.text += " bohSAListenerboh bohlist"+str(count_list)+"-"+directive.get('name')+"boh"
		count_list += 1

	listrel.set('num',str(count_list))
	count_dir += 1


#print prettify(top)

out_file = open(sys.argv[2]+re.sub('xml','data.xml', basename(sys.argv[1])),"w")
out_file.write("<?xml version=\"1.0\" ?>\n"+DTP+prettify(top).strip("<?xml version=\"1.0\" ?>").replace("boh","\""))
out_file.close()

#oks.write('output.xml', encoding="ASCII") #xml_declaration=None, method='xml')









