package tdaq.sa;

public class CHIPAction extends ers.Issue
{
private static final long serialVersionUID = -6862257728765203629L;

public String command;
public String arguments ;

public CHIPAction( final String action, final String parameters )
	    {
	    super("CHIP Action requested: %s %s", action, parameters) ;
	    }
}
