package ch.cern.tdaq.cassandra;

import java.util.Properties;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbcp.BasicDataSourceFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.espertech.esper.compiler.client.CompilerArguments; 
import com.espertech.esper.compiler.client.EPCompilerProvider; 
import com.espertech.esper.common.client.EPCompiled; 
import com.espertech.esper.common.client.util.EventTypeBusModifier ;
import com.espertech.esper.common.client.configuration.Configuration;
import com.espertech.esper.common.client.configuration.common.ConfigurationCommonDBRef ;
import com.espertech.esper.common.client.configuration.common.ConfigurationCommonDBRef.MetadataOriginEnum;
import com.espertech.esper.runtime.client.EPDeployment;
import com.espertech.esper.common.client.EPException;
import com.espertech.esper.runtime.client.EPRuntime;
import com.espertech.esper.runtime.client.EPRuntimeProvider;
import com.espertech.esper.runtime.client.EPStatement;
import com.espertech.esper.runtime.client.EPUndeployException;

import ch.cern.tdaq.cassandra.directive.AlertListener;
import ch.cern.tdaq.cassandra.schema.SADirective;
import ch.cern.tdaq.cassandra.directive.Schema;
import ch.cern.tdaq.cassandra.directive.InvalidDirectiveException;
import ch.cern.tdaq.cassandra.schema.SAInitialStatement;

/**
 * This is a wrapper around the Esper Complex Event Processing engine.
 * <p>
 *
 *  NOTE: By default Esper threading is not enabled. This is to make sure that
 * all processing happen in the calling thread. For example if you send an event
 * and expect to read the results within the same thread.
 *
 *  NOTE: Esper relies on ThreadLocal variables to store transient data. There
 * is a potential of a memory leak if the engine is reset in different thread
 * than the one used for sending events. For example currently all events are
 * sent by the L1 consumer. We need to make sure that we cause the L1 Container
 * to refresh the underlying TaskExecutor (i.e the thread factory) so as older
 * threads are released and all associated ThreadLocal variables are GC-ed
 * properly.
 */
public final class CEPProvider {

	private static final Log log = LogFactory.getLog(CEPProvider.class);
	private static ch.cern.tdaq.cassandra.config.Configuration configuration;
	/*
	 * Thread safe singleton initialization in a static context
	 */
	private static final CEPProvider INSTANCE = new CEPProvider();

	private EPRuntime esperRuntime ;
	// private EPEventService esperEventService ;
	//private EPDeploymentService esperDeployment;
	
	private java.util.Map<String, EPDeployment> deployments = new java.util.HashMap<String, EPDeployment>(); // map of OKS statement ID to the deployment of this statement

  public EPRuntime getEsperRuntime() {
		return esperRuntime;
	}

	public Map<String, EPDeployment> getDeplyments() {
		return deployments ;
	}

//---------------------------------------------------------------------------------
		// Methods for loading, removing and reloading statements and directives
	// compile and deploy EPL statement from a string, return EPDeployment
	public EPDeployment compileDeploy(String epl) {
			try {
				// Build compiler arguments
				final CompilerArguments args = new CompilerArguments(esperRuntime.getConfigurationDeepCopy());
		
				// Make the existing EPL objects available to the compiler
				args.getPath().add(esperRuntime.getRuntimePath());
		
				EPCompiled compiled = EPCompilerProvider.getCompiler().compile(epl, args);
		
// 				System.out.println("Deployed compiled: " + compiled.toString()) ;

				// Return the deployment
				return esperRuntime.getDeploymentService().deploy(compiled);
			}
			catch (Exception ex) {
				throw new RuntimeException(ex);
			}
		}  

	// returns deployment
	public EPDeployment registerStatement(String statement, String statementID) { 
		EPDeployment depl = compileDeploy(statement) ;
		deployments.put(statementID, depl) ; // store, statementID are unique OKS class@object IDs
		log.info("DEPLOYED STATEMENT: "+statementID );
		return depl ;		
		}

// register Initial statements which are not part of any Directive
	public void registerInitStatementsFromDatabase(Schema aalschema) throws config.ConfigException {
		List<SAInitialStatement> configurationStatements = Schema.parseInitialStatements();
		if(configurationStatements!=null){
			for(SAInitialStatement i : configurationStatements) {
				try {
					registerStatement(i.get_code(), i.UID()); // initial statemets do not have listeners, just deploy
				} catch (final RuntimeException e) {
					log.error("Error deploying the initial statement " + i.UID() + ": " + e);
				}
			}
		}
	}

	public void registerDirective(SADirective dir) throws config.ConfigException, InvalidDirectiveException { /***/
		try {
			for (SAInitialStatement initepl : dir.get_initialstatements()) {
				registerStatement(initepl.get_code(), initepl.UID());
				log.info("DEPLOYED INIT EPLSTMT " + initepl.UID() + " for directive " + dir.UID());
			}
			EPDeployment depl = registerStatement(dir.get_epl().get_code(), dir.get_epl().UID());
			log.info("DEPLOYED EPLSTMT: " + dir.get_epl().UID() + " for directive " + dir.UID());
			List<AlertListener> alist = AlertListener.fromListToAlertList(dir.get_listeners(), dir.UID());
			for(AlertListener list: alist)
				{
				depl.getStatements()[0].addListener(list);
				}
		} catch (final RuntimeException e) {
			log.error(dir.UID()+": failed to deploy EPL statements for directive: " + e);
			throw new InvalidDirectiveException(e.getMessage());
		}
	}
	
	public void registerDirectivesFromDatabase(Schema aalschema) throws config.ConfigException {
		Map<String, SADirective> directives = Schema.parseDirectives();
		if(directives!=null){
			for(String stmt: directives.keySet()) {
				try {
					registerDirective(directives.get(stmt));
				} catch ( config.ConfigException | InvalidDirectiveException e) {
						log.error("Directive not loaded: " + e);
				}
			}
		}
	}


	public void destroyAll() {
		try {
			esperRuntime.getDeploymentService().undeployAll();
		}
		catch (final EPUndeployException ex)
			{
			log.error("Failed to undeply a statement: " + ex);
			}
	}

	// undeploy a statement by ID
	public void removeStatement(String id) {
		try {
				log.info("Stopping and removing statement "+ id);
				esperRuntime.getDeploymentService().undeploy(deployments.get(id).getDeploymentId()) ;
		}
		catch (final EPUndeployException ex)
			{
			log.error("Failed to undeply a statement: " + ex);
			}
	}
	
	public void updateStatement(String newStatement, String statementID) {

		removeStatement(statementID);
		
		try {
			registerStatement(newStatement, statementID);
			log.info("Registered new statement " + statementID);
		}
		catch (EPException e) {
			log.error("Failed to register statement " + statementID + ": " + e);
		}
	}
	
	
	public void updateListeners(SADirective dir) throws config.ConfigException {
		
		EPDeployment depl = deployments.get(dir.get_epl().UID()) ;
		if ( depl == null )
			{
			log.error("Deployment not registered for statement " + dir.get_epl().UID());	
			return ;
			}

		//EPStatement statement =	esperRuntime.getDeploymentService().getStatement(depl.getDeploymentId(), dir.get_epl().UID()) ;
		EPStatement[] statements = depl.getStatements() ;
		if ( statements.length == 0 )
			{
			log.error("Deployment " + depl.getDeploymentId() + " of EPL " + dir.get_epl().UID() + " has 0 EPStatements, can not reload");	
			return ;
			}

		log.info("Deployment " + depl.getDeploymentId() + " of EPL " + dir.get_epl().UID() + " has " + statements.length + " statements.")  ;

		statements[0].removeAllListeners() ;

		List<AlertListener> alist = AlertListener.fromListToAlertList(dir.get_listeners(), dir.UID());
		for(AlertListener l: alist)
			{ statements[0].addListener(l); }
		
	}
		
	// type is pre-configured: Message (ERS), ISEvent, Configuration
	public void sendEvent(Object event, final String type) {
		esperRuntime.getEventService().sendEventBean(event, type) ;
  }

	/**
	 * Static method to get the handle to the unique instance of the engine
	 * @return
	 */
	public final static CEPProvider getInstance() {
		return INSTANCE;
	}


	//Private constructor, singleton pattern.
	private CEPProvider() {


	configuration = ch.cern.tdaq.cassandra.config.Configuration.getInstance();


		/****************************
		 *  Setting up the CEP engine
		 ****************************/

		//CEP Configuration.
		//The Configuration is meant only as an initialization-time object.
		Configuration esperConfig = new Configuration();
		java.io.File configFile = new java.io.File(configuration.getEsperConfigurationFile());
		log.info("Loading Esper XML configuration file: " + configFile.getAbsolutePath());
		esperConfig.configure(configFile);
		
		// needed in ESPER-8 to allow access to new evet types etc
		esperConfig.getCompiler().getByteCode().setAllowSubscriber(true);
		esperConfig.getCompiler().getByteCode().setAccessModifiersPublic();
		esperConfig.getCompiler().getByteCode().setBusModifierEventType(EventTypeBusModifier.BUS);
		//FORCE THREAD CONFIGURATION

		//esperConfig.getEngineDefaults().getThreading().setThreadPoolOutbound(true);
		//esperConfig.getEngineDefaults().getThreading().setThreadPoolOutboundNumThreads(20);
		//esperConfig.getEngineDefaults().getThreading().setThreadPoolTimerExec(true);
		//esperConfig.getEngineDefaults().getThreading().setThreadPoolTimerExecNumThreads(20);

		//Setting UP db connection
		Properties props = new Properties();
		
               //Setting UP next db connection
		Properties props2 = new Properties();
		props2.put("username", configuration.getNetlogDBUsername());
		props2.put("password", configuration.getNetlogDBPassword());
		props2.put("driverClassName", "com.mysql.jdbc.Driver");
		props2.put("initialSize", 5);
		props2.put("url", "jdbc:mysql://"+configuration.getNetlogDBURL()+"/");
		props2.put("validationQuery", "select 1 from "+configuration.getNetlogDBTestTable()+" limit 1");

		ConfigurationCommonDBRef configDBNetlog = new ConfigurationCommonDBRef();
		configDBNetlog.setDataSourceFactory(props2, BasicDataSourceFactory.class.getName());
		configDBNetlog.setConnectionLifecycleEnum(ConfigurationCommonDBRef.ConnectionLifecycleEnum.POOLED);

		//MySQL JDBC does not support metadata generation
		configDBNetlog.setMetadataOrigin(MetadataOriginEnum.SAMPLE);
		esperConfig.getCommon().addDatabaseReference("netlog", configDBNetlog);

		esperRuntime = EPRuntimeProvider.getRuntime("AALBrain", esperConfig) ;
	}

	public void stop() {
		esperRuntime.destroy();
	}

}
