package ch.cern.tdaq.cassandra.modification;

import ch.cern.tdaq.cassandra.AALEngine;
import ch.cern.tdaq.cassandra.CEPProvider;


import config.Change;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.util.Arrays;

import com.espertech.esper.common.client.EPException;
import com.espertech.esper.runtime.client.EPDeployment;

import java.lang.String;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.List;

import ch.cern.tdaq.cassandra.schema.SADirective;
import ch.cern.tdaq.cassandra.schema.SAWriter;
import ch.cern.tdaq.cassandra.schema.SAInitialStatement;
import ch.cern.tdaq.cassandra.directive.InvalidDirectiveException;
import ch.cern.tdaq.cassandra.directive.Schema;
import ch.cern.tdaq.cassandra.schema.SAListener;
import ch.cern.tdaq.cassandra.schema.SAConfig;
import ch.cern.tdaq.cassandra.schema.SAConfig_Helper;


public class UpdateCallback implements config.Callback {

	private static final Log log = LogFactory.getLog(UpdateCallback.class);

	private final CEPProvider esper;
	private final AALEngine engine ;

	private config.Configuration db;

	public UpdateCallback(config.Configuration d, AALEngine e) {
		this.db = d;
		this.esper = e.getEsper();
		this.engine = e ;
	}
	
	public void process_changes(config.Change[] changes, java.lang.Object parameter) {

		// the parameter can be any; as an example, the callback ID is passed as string
		String cb_id = (String) parameter;
		log.info(cb_id);

		Map<String, Change> classesChanged = new HashMap<String, Change>();

		// iterate changes by classes
		for(int i = 0; i < changes.length; i++) {

			config.Change change = changes[i];

			log.info("* there are changes in the \'" + change.get_class_name() + "\' class");
			log.info("* objects created: "+ Arrays.toString(change.get_created_objects()));
			log.info("* objects changed: "+ Arrays.toString(change.get_changed_objects()));
			log.info("* objects deleted: "+ Arrays.toString(change.get_deleted_objects()));

			classesChanged.put(change.get_class_name(), change);
		}

		SAConfig updatedConfig ;
		try {
			updatedConfig = SAConfig_Helper.get(db, "main");
		} catch ( final config.ConfigException ex ) {
			ers.Logger.fatal(ex) ; // not really clear what to do in the callback, just check the logs ...
			return ;
		}
		// lock sending new events to CEP engine
		engine.writeLock();
		// Start atomic management unit.
		// Any events concurrently being processed by other threads must complete before the code completes obtaining the lock.
		// Any events sent in by other threads will await the release of the lock.
		try {
		// perform operations such as : - start statements, destroy statements, stop statements - undeploy modules, deploy modules (deployment admin API)
			this.reload(classesChanged, updatedConfig);
		} catch ( final config.ConfigException ex) {
			ers.Logger.error(new ers.Issue("Failed to reload DB changes", ex)) ;
			return ;
		}
		finally {
		// Complete atomic management unit.
		// Any events sent in by other threads will now continue processing against the changed set of statements.
		engine.writeUnlock();		
		}
		}

	public void reload(Map<String, Change> classesChanged, SAConfig updatedConfig) throws config.ConfigException {

		Set<String> changed_writers = new HashSet<String>() ;
		if ( classesChanged.containsKey("SAWriter") )
			{ changed_writers.addAll(Arrays.asList(classesChanged.get("SAWriter").get_changed_objects())) ; } ;

		Map<String, EPDeployment> all_statements = esper.getDeplyments() ;
		List<SAInitialStatement> initstatements ;

		try {
			initstatements = Schema.parseInitialStatements() ;
		} catch (final config.ConfigException ex) {
			ers.Logger.error(new ers.Issue("Failed to reload DB changes", ex)) ;
			return ;
		}
		// first process Initial statements
		for (SAInitialStatement initstmt : initstatements) {
			// new statement, just add
			if ( !all_statements.containsKey(initstmt.UID()) ) {
				try {
					esper.registerStatement(initstmt.get_code(), initstmt.UID());
					log.info("CREATED NEW INITIAL STATEMENT "+initstmt.UID());
				} catch (final RuntimeException e) {
					log.error("Syntax Error in statement: "+e);
				} catch (final config.ConfigException e) {
					log.error("Failed to read statement from DB: "+e);
				}
				
			} else {
				// statement changed, need to reload it
				if (classesChanged.containsKey("SAInitialStatement")) {
					if ( Arrays.asList(classesChanged.get("SAInitialStatement").get_changed_objects()).contains(initstmt.UID())) {
						try {
							esper.removeStatement(initstmt.UID());
							log.info("REMOVED INITSTMT "+initstmt.UID());
							esper.registerStatement(initstmt.get_code(), initstmt.UID());
							log.info("CREATED INITSTMT "+initstmt.UID());
						} catch (final RuntimeException e) {
							log.error("Syntax Error in statement: "+e);
						} catch (final config.ConfigException e) {
							log.error("Failed to read statement from DB: "+e);
						}
					}				
				}
			}	
		}
		
		directive:
		for (SADirective dir : Schema.parseDirectives().values()) { // go throuh all actual directives in DB, there may be changes
			// new directive
			if ( !all_statements.containsKey(dir.get_epl().UID()) ) {
				try {
					esper.registerDirective(dir);
					log.info("CREATED NEW DIRECTIVE "+dir.UID());
				} catch (InvalidDirectiveException e) {
					log.error("Invalid Directive: "+e);
				}
				
			} else {		
				List<String> directivesChanged = new ArrayList<String>();
				
				if ( classesChanged.containsKey("SADirectiveStatement") ) 
				if ( Arrays.asList(classesChanged.get("SADirectiveStatement").get_changed_objects()).contains(dir.get_epl().UID())) {
					try { // remove EPL and registert whole changed directive
						esper.removeStatement(dir.get_epl().UID());
						log.info("REMOVED CHANGED DIRECTIVE "+dir.UID());
						esper.registerDirective(dir);
						log.info("CREATED DIRECTIVE "+dir.UID());
					} catch (InvalidDirectiveException e) {
						log.error("Invalid Directive: "+e);
					}
					directivesChanged.add(dir.UID());
					continue; // next directive
				}
							
				if ( classesChanged.containsKey("SADirective") ) 
				if ( Arrays.asList(classesChanged.get("SADirective").get_changed_objects()).contains(dir.UID())) 
				if ( !directivesChanged.contains(dir.UID()) ) { // if not already realoaded, only need to update listeners
					esper.updateListeners(dir);
					log.info("UPDATED LISTENERS OF DIRECTIVE "+dir.UID());
					directivesChanged.add(dir.UID());
					continue;
				}
				
				for ( SAListener listener : dir.get_listeners() ) {
					// listener changed, reload it
					if ( classesChanged.containsKey("SAListener") ) 
					if ( Arrays.asList(classesChanged.get("SAListener").get_changed_objects()).contains(listener.UID()) )
					if ( !directivesChanged.contains(dir.UID()) ) {
						esper.updateListeners(dir);
						log.info("Listener changed, UPDATED LISTENERS OF DIRECTIVE "+dir.UID());
						directivesChanged.add(dir.UID());
						continue directive ; // directive reloaded, no need to check other listeners
					}									

					for ( SAWriter writer: listener.get_writers() ) {
						if ( changed_writers.contains(writer.UID()) )
						if ( !directivesChanged.contains(dir.UID()) ) {
							esper.updateListeners(dir);
							log.info("Writers changed, UPDATED LISTENERS OF DIRECTIVE "+dir.UID());
							directivesChanged.add(dir.UID());
							continue directive;
						}									
					}				
				}			
			}
		}
				
		for (String statement : esper.getDeplyments().keySet()) {	
			if ( !Schema.allEsperStatementsFromConfig().contains(statement) ) {
				esper.removeStatement(statement);
				log.info("REMOVED INITIALSTATEMENT/DIRECTIVE "+statement);
			}
		}	
	}
}
