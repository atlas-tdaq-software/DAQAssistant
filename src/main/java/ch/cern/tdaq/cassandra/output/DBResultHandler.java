package ch.cern.tdaq.cassandra.output;

import java.sql.*;

import java.util.concurrent.BlockingQueue ;
import java.util.concurrent.LinkedBlockingQueue ;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.cern.tdaq.cassandra.config.Configuration;
import ch.cern.tdaq.cassandra.directive.Alert;

/**
 * This class write alert to Oracle database.
 * Alerts may have only one domain.
 *
 * @author bekoloba
 *
 */

public class DBResultHandler implements OutputHandler {

	private static final Log log = LogFactory.getLog(DBResultHandler.class);
// get DB setting from configuration
	private static final String DB_URL = Configuration.getInstance().getDB_URL();
	private static final String DB_USER = Configuration.getInstance().getDB_USER();
	private static final String DB_PASSWORD = Configuration.getInstance().getDB_PASSWORD();

	private Connection connection = null ;

	private BlockingQueue<Alert> unsent_alerts = new LinkedBlockingQueue<Alert>() ;

	public static DBResultHandler INSTANCE = new DBResultHandler() ;

	private static Connection getNewConnection() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
		DriverManager.setLoginTimeout(10);
		return DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
	}

	private DBResultHandler() {
		try {
			connection = getNewConnection();
			log.info("DBResultHandler created") ;
		}
		catch (SQLException e) {
			e.printStackTrace();
			log.error("SQL Exception: ", e);
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error("Failed to create DBResultHandler: ", e);
		}
		if ( connection == null)
			log.error("Failed to create Oracle connection" ) ;

	}

	@Override
	public void write(Alert alert)
	{	
		if (alert == null) {
			log.error("Received an empty alert to output. What's this?");
			return ;
		}

		real_send(alert) ;

		while (!unsent_alerts.isEmpty()) {
			Alert alert2 = unsent_alerts.poll() ;
			if ( alert2!=null ) {
				log.info("Sending delayed alert " + alert2);
				real_send(alert2) ;
			} ;
		}
	}

	private void real_send(Alert alert)
	{
		PreparedStatement preparedStatement = null;

		sending:
		while ( true )
		{
		try {
			preparedStatement = connection.prepareStatement(
				"insert into assistant_alert "
				+ "(publication_date, name, severity, domain, message, action, details, read_user, read_date, elisa_id) "
				+ "values ("
					+ "sysdate, ?,"
					+ "(select id from assistant_severity where severity=?),"
					+ "(select id from assistant_domain where domain=?),"
					+ "?, ?, ?, NULL, NULL, NULL)");
			if (preparedStatement == null) {
				log.error("The prepareStatement failed.");
				return;
			}
			String name = alert.getName().trim();
			preparedStatement.setString(1, name.substring(0, Math.min(name.length(), 50)));
			String sev = alert.getSeverity().trim();
			preparedStatement.setString(2, sev.substring(0, Math.min(sev.length(),20)));
			String dom = alert.getDomain().trim();
			preparedStatement.setString(3, dom.substring(0, Math.min(dom.length(),50)));
			String msg = alert.getMessage().trim();
			String submsg;
			if(msg.length()>996) {
				submsg = msg.substring(0,996) + "...";
			}
			else submsg = msg;
			preparedStatement.setString(4, submsg);
			String act = alert.getAction().trim();
			preparedStatement.setString(5, act.substring(0, Math.min(act.length(),1000)));

			String details = AlertFormatter.getAlertDetails(alert);
			String subdet;
			if(details.length()>3996) {
				subdet = details.substring(0,3996) + "...";
			}
			else subdet= details;
			
			preparedStatement.setString(6, subdet);

			preparedStatement.executeUpdate();
		}
		catch ( final SQLRecoverableException ex ) // Closed connection, need to re-establish connection // e.g. in case of oracle server restart
			{	
			log.info("Got recoverable Oracle exception " + ex + ". Will re-establish connection to server and re-try executing SQL Statement") ;
			try { connection.close(); preparedStatement.close(); }
			catch ( final Exception ex2 ) 		{ log.error("Failed to clean up: " + ex2) ; } 

			for ( int att=0; att < 2; att++)
				{
				try {
					Thread.sleep((att+1)*1000) ;
					connection = getNewConnection() ;
					continue sending; // success, retry creating preparedStatement and executeUpdate
					}
				catch ( final Exception ex3 ) // too bad
					{
					log.error("Can not re-establish connection in SQLRecoverableException, attempt " + att + ": "  + ex3) ;
					}
				}		
				// give up with this alert, store it in memory and re-send at next occasion
			log.error("Failed to re-establish connection in SQLRecoverableException: " + ex) ;

			try {
				unsent_alerts.put(alert) ;
				log.warn("Alert " + alert.getName().trim() + " not sent, it put into a queue and will be sent later") ;
			}
			catch (final InterruptedException exx) { } ; // can not happen

			break sending;
			}
		catch ( SQLException ex ) {
		    if (ex.getSQLState().equals("23000")) {
				// State 23000 means "The parameter is not nullable".
				// In our case we tried to insert a severity or domain that doesn't exist
				// in the severity or domain table and we got NULL from the subquery.
				// We need to insert it first into the severity or domain table and re-execute the query.
			    PreparedStatement tryToInsertSeverity = null;
			    PreparedStatement tryToInserDomain = null;
				try {
					tryToInsertSeverity = connection.prepareStatement(
						"INSERT INTO assistant_severity (severity) "
						+ "SELECT ? "
						+ "FROM dual "
						+ "WHERE NOT EXISTS (SELECT NULL FROM assistant_severity WHERE severity = ?)");

					if ( tryToInsertSeverity == null) {
						log.error("The prepareStatement failed.");
						return;
					}

					tryToInsertSeverity.setString(1, alert.getSeverity().trim());
					tryToInsertSeverity.setString(2, alert.getSeverity().trim());
					tryToInserDomain = connection.prepareStatement(
						"INSERT INTO assistant_domain (domain) "
						+ "SELECT ? "
						+ "FROM dual "
						+ "WHERE NOT EXISTS (SELECT NULL FROM assistant_domain WHERE domain = ?)");

					if ( tryToInserDomain == null) {
						log.error("The prepareStatement failed.");
						return;
					}

					tryToInserDomain.setString(1, alert.getDomain().trim());
					tryToInserDomain.setString(2, alert.getDomain().trim());

					tryToInsertSeverity.executeUpdate();
					tryToInserDomain.executeUpdate();
					preparedStatement.executeUpdate();
				} catch ( SQLException excep ) {
					ex.printStackTrace();
					log.error("Failed to insert Severity or Domain column into Oracle: " + excep ) ;
				} finally {
					try {
						tryToInsertSeverity.close();
						tryToInserDomain.close();
					}
					catch ( SQLException except ) {
						ex.printStackTrace();
						log.error("Failed to close PreparedStatement for Severity or Domain insert: " + except ) ;
					}
				}
		    }
		    else {
			ex.printStackTrace();
			log.error("Failed to write alert to Oracle: " + alert.getName() + ": " + alert.getMessage() +  " *** " + ex ) ;
			try {
				unsent_alerts.put(alert) ;
				log.warn("Alert " + alert.getName().trim() + " not sent, it put into a queue and will be sent later") ;
			}
			catch (final InterruptedException exx) { } ; // can not happen
		    }
		} // try-catch SQL
		finally {
			if (preparedStatement != null) {
				try { preparedStatement.close(); }
				catch ( SQLException ex ) {
					ex.printStackTrace();
					log.error("Failed to close PreparedStatement: " + ex ) ;
				}
			}
		}
		break ; // do not retry on SQLException, break the while (true) loop
		} // while (true) try-catch
	}

	public static OutputHandler loadFromXML() {
		return INSTANCE ;
	}
}
