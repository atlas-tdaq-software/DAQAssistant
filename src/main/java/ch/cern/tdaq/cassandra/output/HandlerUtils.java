package ch.cern.tdaq.cassandra.output;

import java.util.StringTokenizer ;
import java.util.NoSuchElementException ;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.espertech.esper.common.client.EventBean;

public class HandlerUtils {

	private final static String varDelimiter = "$";
	private static final Log log = LogFactory.getLog(HandlerUtils.class);
	
	// function which replaces $VAR$ in the alert text using property of an event (usually the first and the only event)
	public static String parseVariables(String input, EventBean[] events)
	{
	log.trace("Parsing output text " + input) ;
	if ( events == null || events.length == 0 )
		{
		log.trace("No events passed for resolving the variables for text " + input);
		return input ;
		}
//	if ( event.length > 1 )
//		{
//		log.trace("More then one event (" + event.length + ") passed for resolving the variables: check what you expect in your directive! No substitution is done in the text: " +
//		input);
//		return input ;
//		}

	try	{
			
		//Resolve input dynamic properties
		StringTokenizer tokens = new StringTokenizer(input,varDelimiter,true) ;
		StringBuffer output = new StringBuffer();
		while ( tokens.hasMoreTokens() )
			{
			String el = tokens.nextToken() ;
			// log.trace("Parsing token " + el);
			if ( el.equals(varDelimiter) )
				{
				String var = tokens.nextToken() ;

				log.trace("Resolving variable " + var);
				try 	{
/*					if ( event.length > 1 )
						{
						log.error("More then one event (" + event.length + ") passed for resolving the variables: check what you expect in your directive! No substitution is done in the text: " +
						input);
						output.append(varDelimiter).append(var).append(varDelimiter) ;
					else
						}*/
					if ( events.length == 1 )
						output.append(events[0].get(var)) ; // resolve $variable$
					else
						{
						output.append('[') ;
						int i = 0 ;
						for ( EventBean event: events )
							{
							if ( i!=0 ) { output.append(", ") ;} ;
							output.append(event.get(var));
							if ( i++ > 8 ) // limit output to a reasonable number of events
								{
								output.append("..+ more events skipped") ;
								break ;
								}
							}
						output.append(']') ;
						}
					}
				catch (com.espertech.esper.common.client.PropertyAccessException e) {
					log.error("No property with name " + var + " found in the event!");
					output.append(varDelimiter).append(var).append(varDelimiter) ;
				}

				tokens.nextToken() ; // skip trailing $
				}
			else	
				output.append(el) ;
			}

		return output.toString() ;

		} catch (NoSuchElementException e) {
			log.error("Malformed use of variable in alert attributes: " + input);
		}

	return input ;
	}	


}
