package ch.cern.tdaq.cassandra.output;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.cern.tdaq.cassandra.config.Configuration;
import ch.cern.tdaq.cassandra.directive.Alert;
import ch.cern.tdaq.cassandra.schema.CHIPAction;
import daq.es.OnDemandAction ;

public class CHIPActionHandler implements OutputHandler {

	private static final Log log = LogFactory.getLog(FileResultHandler.class);
	private Configuration config = Configuration.getInstance();
    	private String command  ;

	public CHIPActionHandler(CHIPAction action) { 
		try { command = action.get_command() ; }
		catch (final config.ConfigException ex) {
			log.error(ex) ;
			command = "undefined" ;
		}
	}

	@Override
	public void write(Alert alert) {
		try {
		log.info("CHIPActionHandler: sending " + command + " " + alert.getMessage() + " to CHIP") ;
        is.Repository rep = new is.Repository(new ipc.Partition(config.getCHIPActionPartition())) ;
        daq.es.OnDemandAction action = new daq.es.OnDemandAction() ;
        action.command = command ;
        action.arguments = alert.getMessage().replaceAll("\\[|\\]","") ; // remove square brackets around comma-separated list of arguments
        rep.checkin(config.getCHIPActionInfoName(), action) ;
        }
        catch (Exception ex) {
			log.error("Failed to write alert " + alert.getName() + " to IS: " + ex) ;
		}
	}
}
