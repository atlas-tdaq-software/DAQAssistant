package ch.cern.tdaq.cassandra.output;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.File;
import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.cern.tdaq.cassandra.config.Configuration;
import ch.cern.tdaq.cassandra.directive.Alert;

public class FileResultHandler implements OutputHandler {

	private static final Log log = LogFactory.getLog(FileResultHandler.class);
	private Configuration config = Configuration.getInstance();

	private String filename;
	private boolean append = true ;
	private final Format format;

	public FileResultHandler(String file, boolean append ) {

		this.format = Format.XML ;
		this.append = append;

		filename = config.getResultDirectory()+"/"+file;
		try {
			(new File(config.getResultDirectory())).mkdirs() ;

			//To remove existing file
			if ( append == false )
				{
				BufferedWriter out = new BufferedWriter(new FileWriter(filename, false));
				out.close();
				}
		} catch (IOException e) {
			e.printStackTrace() ;
		}
	}

	public FileResultHandler(String file) { this(file, true) ; } 

	@Override
	public void write(Alert alert) {

		String alert_representation;

		alert_representation = AlertFormatter.decodeAlert(alert, format);

		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(filename, true));
			out.write(alert_representation);
			out.write("\n");
			out.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
