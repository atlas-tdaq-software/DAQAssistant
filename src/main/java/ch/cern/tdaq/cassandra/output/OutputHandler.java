package ch.cern.tdaq.cassandra.output;

import ch.cern.tdaq.cassandra.directive.Alert;

public interface OutputHandler{
	public void write(Alert alert);
}
