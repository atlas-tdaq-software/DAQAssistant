package ch.cern.tdaq.cassandra.output;

import java.io.StringWriter;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.espertech.esper.common.client.EventBean ;

import ch.cern.tdaq.cassandra.CEPProvider;
import ch.cern.tdaq.cassandra.directive.Alert;

/**
 * Utility class to produce a string representation of an Alert {@code ch.cern.tdaq.cassandra.directive.Alert})
 * Alert can be decoded in different format: XML, TEXT, HTML.
 * @author lmagnoni
 *
 */
public class AlertFormatter {

	private static final Log log = LogFactory.getLog(AlertFormatter.class);

	private AlertFormatter(){
		//To inhibit public constructor...
	}
	
	private static long getCurrentTimestamp() {
		return CEPProvider.getInstance().getEsperRuntime().getEventService().getCurrentTime();
	}

	public static String sanitizeXmlAttrName(String property) {
		/* Valid XML attr name: [a-zA-Z_:][-a-zA-Z0-9_:.] */
		
		if (property == null || property.length() == 0) {
			return "";
		}
		
		StringBuilder b = new StringBuilder();
		int i;
		
		for (i = 0; i < property.length(); ++i) {
			char c = property.charAt(i);
			if (((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c == '_' || c == ':')) {
				b.append(c);
			} else {
				String s = String.format("x%02x", Character.codePointAt(property, i));
				b.append(s);
			}
		}
		
		return b.toString();
	}
	
	/**
	 * This it the only public method in this class. Client provides the alert and 
	 * the Format ({@code ch.cern.tdaq.cassandra.output.Format}) they want to use for decoding.
	 * 
	 * @param alert
	 * @param format
	 * @return a string representation in the desired format
	 */
	public static String decodeAlert(Alert alert, Format format) {

		switch(format) {

		case TEXT: {

			StringBuffer buffer = new StringBuffer();

			//Add the mandatory alert information
			buffer.append("alert: "+alert.getName()+"\n");
			buffer.append("domain: "+alert.getDomain()+"\n");
			buffer.append("severity: "+alert.getSeverity()+"\n");
			buffer.append("message: "+alert.getMessage()+"\n");
			buffer.append("action: "+alert.getAction()+"\n");
			buffer.append("details:");
			buffer.append(getAlertDetails(alert));
			return buffer.toString();

		}

		case XML: {

			StringWriter res = new StringWriter();
			XMLStreamWriter writer;
			try {
				writer = XMLOutputFactory.newInstance().createXMLStreamWriter(res);
				writer.writeStartDocument();
				writer.writeStartElement("simplealert");
				//Domain
				writer.writeAttribute("domain", alert.getDomain());
				//Name
				writer.writeAttribute("name", alert.getName());
				//Severity
				writer.writeAttribute("severity", alert.getSeverity().replaceAll(" ", ""));
				//Message
				writer.writeAttribute("alert", alert.getMessage());
				//Action
				writer.writeAttribute("action", alert.getAction());
				//Timestamp (used by SAReplay)
				writer.writeAttribute("timestamp", Long.toString(getCurrentTimestamp()));
				
				//Details
				writer.writeStartElement("events");
				for(EventBean e:alert.getNewEvents()) {
					//Get list of event properties
					String[] properties = e.getEventType().getPropertyNames();

					writer.writeStartElement("event");

					for(String property: properties) {
						/**
						 * @ todo finire di inserire i vari tipi base qui!
						 */
						//log.debug( e.getEventType().getPropertyType(property).getName());

						if(e.getEventType().getPropertyType(property) != null ){

							String type = e.getEventType().getPropertyType(property).getName();
							Object event = e.get(property);
							try {
								String propertyNameSane = sanitizeXmlAttrName(property);
								
								if (event != null) {
									writer.writeAttribute(propertyNameSane,  decodeEvents(property, type, event));
								} else {
									writer.writeAttribute(propertyNameSane, "null");
								}
							} catch (InvalidEventPropertyException ex) {
								log.warn("Error decoding event property: "+property+":"+type+" - "+ex.getMessage());
							} catch (RuntimeException ex) {
								log.error("RuntimeException decoding the property "+property+" of type "+type+": properties="+Arrays.asList(properties)+" eventBean="+e, ex);
								//throw ex;
							}
						}
					}

					writer.writeEndElement();
				}

				writer.writeEndElement();
				writer.writeEndElement();
				writer.writeEndDocument();

			} catch (XMLStreamException e1) {
				e1.printStackTrace();
			} catch (FactoryConfigurationError e1) {
				e1.printStackTrace();
			}

			return res.toString();
		}

		case HTML: {

			StringWriter res = new StringWriter();
			res.write("<html> <body> <b>Domain: " + alert.getDomain() + "</b>");
			res.append("<p><b>Name :  </b> " + alert.getName() + "</p>");
			res.append("<p><b>Severity :</b> " + alert.getSeverity() + "</p>");
			res.append("<p><b>Message :</b> " + alert.getMessage() + "</p>");
			res.append("<p><b>Action :</b> " + alert.getAction() + "</p>");
			//res.append("<p><b>Timestamp :</b> " + getCurrentTimestamp() + "</p>");
			res.append("<p><b>Details :</b>  Details: </p>");

			for(EventBean e:alert.getNewEvents()) {
				//Get list of event properties
				String[] properties = e.getEventType().getPropertyNames();
				res.append("<p> <b>Event</b>:");	

				for(String property: properties) {
					/**
					 * @ todo finire di inserire i vari tipi base qui!
					 */
					//log.debug( e.getEventType().getPropertyType(property).getName());

					if(e.getEventType().getPropertyType(property) != null ){
						String type = e.getEventType().getPropertyType(property).getName();

						Object event = e.get(property);
						try {
							res.append(" "+property+":"+decodeEvents(property, type, event)+" ");
						} catch (InvalidEventPropertyException ex) {
							log.warn("Error decoding event property: "+property+":"+type+" - "+ex.getMessage());
						}
					}

				}
				res.append("</p>");	
			}
			res.write("</body> </html>");
			return res.toString();

		}

		default: {

			//TEXT
			StringBuffer buffer = new StringBuffer();

			//Add the mandatory alert information
			buffer.append("alert: "+alert.getName()+"\n");
			buffer.append("domain: "+alert.getDomain()+"\n");
			buffer.append("severity: "+alert.getSeverity()+"\n");
			buffer.append("message: "+alert.getMessage()+"\n");
			buffer.append("action: "+alert.getAction()+"\n");
			//buffer.append("timestamp: " + getCurrentTimestamp() + "\n");
			//Add the dynamic attributes
			buffer.append("details: ");
			buffer.append(getAlertDetails(alert));
			return buffer.toString();

		}

		}

	}

	/**
	 * The alert details represent
	 * the condition that trigger a specific alert, in form of EventBean[] from Esper.
	 * This method is used to decode the details in a proper representation
	 * @param alert
	 * @return
	 */
	public static String getAlertDetails(Alert alert) {

		StringBuffer buffer = new StringBuffer();

		//Add the events detail if presents
		if(alert.getNewEvents()==null)
			buffer.append("--\n");
		else {
			//buffer.append("details:"+"\n");			
			for(EventBean e:alert.getNewEvents()) {
				//Get list of event properties
				String[] properties = e.getEventType().getPropertyNames();

				buffer.append("<br>");

				for(String property: properties) {

					String type = null;
					if((e!=null) && (e.getEventType().getPropertyType(property)!= null))
						type = e.getEventType().getPropertyType(property).getName();
					else
						continue;

					Object event = e.get(property);

					try {
						if (event != null) {
							buffer.append(property+": "+decodeEvents(property, type, event)+" ");
						} else {
							buffer.append(property+": null ");
						}
					} catch (InvalidEventPropertyException ex) {
						log.warn("Problem decoding the property "+property+" of type "+type+": "+ex);
						buffer.append(property+": "+ e.getEventType().getPropertyType(property).getName() +" ");
					} catch (RuntimeException ex) {
						log.error("RuntimeException decoding the property "+property+" of type "+type+": properties="+Arrays.asList(properties)+" eventBean="+e, ex);
						//throw ex;
					}
				}
			}
		}
		return buffer.toString();
	}


	/**
	 * Esper return a Map of <String, Object> properties with the data gathered by the EPL statements.
	 * This function provide a smart way to get a string representation of the properties,to be appended to
	 * the text, xml, or HTML representation of the alert.
	 * Reflection is used to introspect the Object class. Different actions are taken in case it's an Array or a Set.
	 * 
	 * @param name
	 * @param type
	 * @param event
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static String decodeEvents(String name, String type, Object event) throws InvalidEventPropertyException {

		/**
		 * Using reflection to cast the property Object to the proper class.
		 * For every type of object, the Java virtual machine instantiates an immutable instance of java.lang.Class.
		 * It can be used to access the reflection API.
		 */

		if (event == null) {
			return "null";
		}
		
		try {
			
			Class<?> clazz;

			/**
			 * Primitive types cannot be retrieved via Class.forName.
			 * A look-up on the possible primitive types is needed.
			 * The code leverages autoboxing capability, so that the retrieved class
			 * is directly the boxed type.
			 */
			
			if(type=="byte")
				clazz = Byte.class;
			else if(type=="short") 
				clazz = Short.class;
			else if(type=="int") 
				clazz = Integer.class;
			else if(type=="long") 
				clazz = Long.class;
			else if(type=="float") 
				clazz = Float.class;
			else if(type=="double") 
				clazz = Double.class;
			else if(type=="boolean")
				clazz = Boolean.class;
			else if(type=="char") 
				clazz = Character.class;
			else
				clazz = Class.forName(type);

			//Deal with Object array
			if(clazz.isArray()) {

				int len = Array.getLength(event);
				StringWriter buffer = new StringWriter();
				buffer.append(name+":[ ");
				for(int i=0;i<len;i++)
					buffer.append(Array.get(event, i)+", ");
				buffer.append("]\n");
				return buffer.toString();

				//Inner properties can be Map of values. Iterate with keyset
			} else if(clazz.getSuperclass()!=null && clazz.getSuperclass().getName().equals("java.util.Map")) {

				/**
				 * For performance reason I cast inner properties to a <String, Object> map,
				 * since I know Esper creates maps like that.
				 * This save the usage of reflection for calling methods, that can bring 
				 * to huge performance degradation. 
				 */
				Map<String, Object> m = (Map<String, Object>) event;
				if(m!=null) {
					StringWriter buffer = new StringWriter();
					buffer.append(name+":{ ");
					for(String param:m.keySet())
						buffer.append(param+": "+ m.get(param)+", ");
					buffer.append("}\n");
					return buffer.toString();
				}
			} else {
				//Simplest case. Calling the string representation of the object itself
				return clazz.cast(event).toString();
			}

		} catch (ClassNotFoundException e) {
				throw new InvalidEventPropertyException("ClassNotFound"+ e.getMessage());
		}
    return "null" ; //never here
	}


	public static void main(String [] args) {
		// TODO JUNIT test

		String s1 = "string";
		String[] sa = {"s1, s2"};
		Integer integer = new Integer(1);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("uno", 0);
		map.put("due", "cippas");

		try {
			System.out.println("String: "+AlertFormatter.decodeEvents("string", "java.lang.String", s1));
			System.out.println("StringArray: "+AlertFormatter.decodeEvents("stringa", "[Ljava.lang.String;", sa));
			System.out.println("Integer: "+AlertFormatter.decodeEvents("Integer", "java.lang.Integer", integer));
			System.out.println("Map: "+AlertFormatter.decodeEvents("Map", "java.util.Map", map));
			System.out.println("Boolean: "+AlertFormatter.decodeEvents("boolean", "boolean", false));
		} catch (InvalidEventPropertyException ex) {
			log.warn("Error decoding event property: "+ex.getMessage());
		}
	}
}
