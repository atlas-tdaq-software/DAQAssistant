package ch.cern.tdaq.cassandra.output;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.cern.tdaq.cassandra.directive.Alert;
import ch.cern.tdaq.cassandra.schema.SAErs;

public class ERSResultHandler implements OutputHandler {
	
	private static final Log log = LogFactory.getLog(ERSResultHandler.class);
	private final String severity ;
	private final String name ;
	//private final String chip_command ; // for CHIP action, e.g. KILL_APPS. Parameters of the command to be taken from the alert
	private List<String> qualifiers ;

	public ERSResultHandler(SAErs ers, String dirname) throws config.ConfigException {
		this.name = dirname ;
		this.severity = ers.get_severity();
		this.qualifiers = Arrays.asList(ers.get_qualifiers()) ;
	//	chip_command = "" ;
	}

/* 	public ERSResultHandler(CHIPAction action, String dirname) {
		this.name = dirname ;
		this.severity = action.get_severity();
		this.qualifiers = Arrays.asList(action.get_qualifiers()) ;
		chip_command = action.get_command() ;
		// FIXME handle empty command? Or protect it in schema?
	} */
	
	@Override
	public void write(Alert alert) {
		log.trace(name + ": sending ERS message " + alert.getName());
		
		ers.Issue issue = new SAIssue(alert.getName(), alert.getMessage(), alert.getAction()) ;

		//if ( chip_command.isEmpty() )
		// { issue = new SAIssue(alert.getName(), msg_txt, alert.getAction()) ; }
		//else
		//	{	// a specific Issue expected by CHIP, with 2 ERS parameters: command name and command parameters(comma-separated)
		//	issue = new tdaq.sa.CHIPAction(chip_command, alert.getMessage().replaceAll("\\[|\\]","")) ;
		//	}
			
		issue.addQualifier("ShifterAssistant") ;
		issue.addQualifier(alert.getDomain()) ;
		for ( String aq : qualifiers ) issue.addQualifier( aq ) ;	

		if ( severity.equals("FATAL") )
			{
			ers.Logger.fatal(issue);
			}
		else if ( severity.equals("ERROR") )
			{
			ers.Logger.error(issue); 
			}
		else if ( severity.equals("WARNING") )
			{
			ers.Logger.warning(issue); 
			}
		else if ( severity.equals("INFORMATION") )
			{
			ers.Logger.info(issue); 
			}
		else 
			{
			log.error("Unsupported  severity " + severity +  " in alert " + alert.getName());
			ers.Logger.info(issue) ;
			}
	}

}
