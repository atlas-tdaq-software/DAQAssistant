package ch.cern.tdaq.cassandra.output;

public enum Format { 
	
	XML("xml"), TEXT("text"), HTML("html");
	
	private final String name;
	
	//constructor
	Format(String name) {
		this.name = name;
	}
	
	
}

