package ch.cern.tdaq.cassandra.output;

class SAIssue extends ers.Issue
{
public String name ;
public String message ;
public String action ;

public SAIssue( final String name, final String message, final String action )
	    {
	    super("SA ALERT %s: %s, required action: %s", name, message, action) ;
	    }
}
