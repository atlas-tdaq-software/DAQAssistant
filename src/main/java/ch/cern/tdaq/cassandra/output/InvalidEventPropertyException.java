package ch.cern.tdaq.cassandra.output;

public class InvalidEventPropertyException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 /**
     * Ctor.
     * @param message - error message
     */
    public InvalidEventPropertyException(final String message) {
        super(message);
    }

}
