package ch.cern.tdaq.cassandra.output;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.cern.tdaq.cassandra.config.Configuration;
import ch.cern.tdaq.cassandra.directive.Alert;
import ch.cern.tdaq.cassandra.schema.SAEmail;

import java.util.Arrays;


public class MailResultHandler implements OutputHandler {
	
	private static final Log log = LogFactory.getLog(MailResultHandler.class);
	private final String name;
	private List<String> recipients ;
	
	public MailResultHandler(SAEmail email, String dirname) throws config.ConfigException {
		this.recipients = Arrays.asList(email.get_addresses());
		this.name = dirname;
	}
		
	@Override
	public void write(Alert alert) {
		
		log.info("Sending email...");
		
		String msg_txt = alert.getMessage() + "\nSuggested action: " + alert.getAction() + "\nDetails:\n"
			+ AlertFormatter.getAlertDetails(alert).replaceAll("<br>","\n") ;
	
		log.debug(name+" Sending EMail message " + name + " to " + String.join(";", recipients));

		String mailcmd = "echo \"" + msg_txt + "\"" + "| /bin/mail -r " + Configuration.getInstance().getEmailFrom() + " -s \"SA alert:" + alert.getSeverity().trim() + ": " + name + "\" " ;

		for ( int i = 0 ; i < recipients.size() ; i++ ) mailcmd += recipients.get(i) + " ";

		String[] cmd = {"/bin/sh", "-c", mailcmd};

		log.trace("Executing " + mailcmd) ;

		try {
			Process p = Runtime.getRuntime().exec(cmd) ;
			String line;
      			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
      			while ((line = input.readLine()) != null) {
        		System.err.println(line);
      			}
      			input.close() ;
		}
		catch ( Exception ex )
			{
			log.error("Failed to send email: " + ex ) ;
			}
	}
	
	
//	public static OutputHandler loadFromXML(String name,boolean details, Element element) {
		
		//list or receipients
//		Vector<String> recipients = new Vector<String>() ;
//		for ( int i = 0; i < element.getElementsByTagName("recipient").getLength(); i++ ) {
//			Element  xpart = (Element) element.getElementsByTagName("recipient").item(i);
//			recipients.add(xpart.getFirstChild().getNodeValue());
//			log.debug("Recipient address parsed:" + xpart.getFirstChild().getNodeValue());
//		}
//		
//		//Parsing format
//		String severity = "WARNING";
//		if(element.getElementsByTagName("severity").getLength()!=0) {
//			Element  xseve = (Element) element.getElementsByTagName("severity").item(0);
//			severity = xseve.getFirstChild().getNodeValue();
//			log.debug("Severity parsed:"+severity);
//		}
//				
//		return new MailResultHandler(name, recipients, severity, details);
//	}
//
//
}
