package ch.cern.tdaq.cassandra;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.cern.tdaq.cassandra.event.ConfigurationEvent;
import ch.cern.tdaq.cassandra.injector.Injector;
import ch.cern.tdaq.cassandra.injector.Injectors;
import ch.cern.tdaq.cassandra.reader.ConfigReader;

import ch.cern.tdaq.cassandra.directive.Schema;
import ch.cern.tdaq.cassandra.modification.UpdateCallback;

import static java.util.concurrent.TimeUnit.*;

/**
 * This is the main class for the AAL engine.
 * <p>
 * It coordinates the interaction between the CEP engine and the TDAQ infrastructure, via injectors and directives.
 * <p>
 * The CEP engine is controlled via the wrapper class CEPProvider ({@link ch.cern.tdaq.cassandra.CEPProvider}).
 * At creation time it initializes the Injectors ({@link ch.cern.tdaq.cassandra.Injector}) as specified in the configuration
 * file cassandra.properties , and it creates the list of Directives ({@link ch.cern.tdaq.cassandra.directive.Directive}).
 * Since new Injectors may appear at run time (as the case of new IS server matching regexp), a polling mechanism is implemented
 * to check for new Injectors.
 * <p>
 */


//The engine is a final class, so it cannot be further extended or subclassed.
//This allow for performance optimization at compile time.
public final class AALEngine {

	private static final Log log = LogFactory.getLog(AALEngine.class);

	//Creating a configuration useful for loading directives and initial statements
	private config.Configuration db;
	private Schema AALSchema;

	/**
	 * Thread safe singleton initialization in a static context
	 */
	private static final AALEngine INSTANCE = new AALEngine();

	/**
	 * The {@link ch.cern.tdaq.cassandra.CEPProvider} is the handler for the CEP engine
	 */
	private final CEPProvider esper = CEPProvider.getInstance();

	/**
	 * System configuration information, from properties file
	 */
	private static ch.cern.tdaq.cassandra.config.Configuration configuration;

	/**
	 * Set of injectors
	 */
	private Map<String, Injector> injectors;

	/**
	 * Pool of thread used for speed up CORBA's callback
	 * When injectors (IS or MRS) receive a callback, they create and add a task into the non blocking pool thread queue.
	 * A thread from the pool execute the task and it will deal with the insertion of the event in Esper.
	 * IMPORTANT
	 * Esper by default use the same application thread that invokes any
	 * of the {@code sendEvent} methods to process the event fully and also deliver output events to listeners and subscribers.
	 * This can be tuned using the inbound and outbound threading options.
	 */
	private static final int NTHREADS = ch.cern.tdaq.cassandra.config.Configuration.getInstance().getCallbackThreads();
	private static final ExecutorService injectorCallbackPool = Executors.newFixedThreadPool(NTHREADS);


	/**
	 * Pool of thread used for 2 periodic actions:
	 * - Check for New IS
	 * - Check for IS server status and consequentially stop/start injectors
	 */
	 private final ScheduledExecutorService periodicScheduler =  Executors.newScheduledThreadPool(2);

	private String rootWriter = "";
	
	
	private final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
	
	// multithreading protection for dynamic reloading of EPL statements and sending events from different threads
	private final Lock read = readWriteLock.readLock();
	private final Lock write = readWriteLock.writeLock();
	
	public final void readLock() { read.lock() ; }
	public final void readUnlock() { read.unlock() ; }
	public final void writeLock() { write.lock() ; }
	public final void writeUnlock() { write.unlock() ; }

	/**
	 * Static method to get the handle to the unique instance of the engine
	 * @return
	 */
	public final static AALEngine getInstance() {
		return INSTANCE;
	}

	/**
	 * Static method to get the handle to the unique instance of the Esper
	 * @return
	 */
	public final CEPProvider getEsper() {
		return esper;
	}

	private AALEngine() {

		configuration = ch.cern.tdaq.cassandra.config.Configuration.getInstance();

		//Using HashMap. O(1) for retrieving data. Non synchronized.
		injectors = new HashMap<String, Injector>();


		/**
		 * Registering a new JVM shutdown hook for this thread.
		 * The @ShutdownHook thread contains the clean up code
		 * for connections and subscriptions.
		 */
		ShutdownHook shutdownHook = new ShutdownHook();
	    Runtime.getRuntime().addShutdownHook(shutdownHook);

	}

	public void configure() {

		log.info("Configuration phase started...");
		log.info("Reading configuration: "+ configuration.toString());

		Injector replayInjectorInstance = null;
		if (configuration.getReplayMode()) {
			try {
				/*
				 * We're creating the object here since on the ReplayInjector constructor
				 * there is basic initialization, specifically the ConfigReader initialization.
				 * We need to do that before the directives are loaded.
				 */
				log.info("Configuring ESPER in Replay mode, calling replay.ReplayInjector constructor");
				replayInjectorInstance = (Injector)(Class.forName("ch.cern.tdaq.cassandra.replay.ReplayInjector")).newInstance();
				log.info("ReplayInjector constructed, continue with ESPER configuration");
				// ReplayInjector may throw RuntimeException, catch all that in Main, no way to continue
			} catch (ClassNotFoundException e) {
				throw new RuntimeException(e) ;
			} catch (InstantiationException e) {
				throw new RuntimeException(e) ;
			} catch (IllegalAccessException e) {
				throw new RuntimeException(e) ;
			}
		}

		//esper.registerStatement("create schema MyEvent(state string, partitionName string);", "stmt0") ; 

		// String text = "insert into PartitionState select * from MyEvent" ;
		// "insert into ATLASState select state as status from PartitionState" ;
		// esper.registerStatement("create schema MyEvent(state string, partitionName string); create context AtlasRunning start ISEvent(state='RUN') stop ISEvent(state='STOP')", "stmt1");
		//esper.registerStatement("insert into PartitionState select * from MyEvent", "stmt1");
		//esper.registerStatement("insert into ATLASState select state as status from PartitionState", "stmt2") ; 
		// if ( true ) return ;

		/**
		 * Creating an RDB or an OKS configuration
		 */
		log.info("Loading OKS/RDB directives configuration: " + configuration.getRDBConfig());
		try {
			db = new config.Configuration(configuration.getRDBConfig());	 
		AALSchema = new Schema(db);
		
		/**
		 * Setting up basic statements
		 */
		log.info("Registering initial statements... ");
		esper.registerInitStatementsFromDatabase(AALSchema);

		ConfigurationEvent c = new ConfigurationEvent("INIT");
		//Blocking creation of the INIT event on Esper
		//This thread will do the processing as wellcom.espertech.esper.Timer-AALBrain-0
		esper.sendEvent(c, "Configuration");

		/**
		 *  Setting Root Writer if any
		 */
		//Set the RootWriter. Null if any.
		rootWriter = configuration.getDirectiveWriterMask();
		log.info("Directives Writers mask set to:"+rootWriter);
		
		/**
		 * Setting up directives
		 */
		log.info("Parsing directives... ");
		esper.registerDirectivesFromDatabase(AALSchema);
		} catch (final config.ConfigException ex) {
			throw new RuntimeException(ex) ;
		}

	//	log.info(Arrays.toString(esper.getStatName()));

		esper.sendEvent( new ConfigurationEvent("DIR"), "Configuration");


		/************************
		 * Starting Injectors
		 ************************/

		if (configuration.getReplayMode()) {
			log.info("Starting Replay injector...");
			injectors.put("replayinjector", replayInjectorInstance);
		} else {
			log.info("Parsing MRS injectors...");
			injectors.putAll(Injectors.parseERSInjectorFromConfig());

			//Load IS injectors
			log.info("Parsing IS injectors...");
			injectors.putAll(Injectors.parseISInjectorFromConfig());

			//Load IS injectors
			log.info("Parsing Dynamic IS injectors...");
			injectors.putAll(Injectors.parseSmartISInjector());
		}

		log.info("Configuration phase done.");
	}


	public void start() {

//		/**
//		 * Setting up post statements
//		 */
//		List<Directive> postStatements = XMLUtils.parsePostStatementsFromXML();
//		for(Directive stmt: postStatements) {
//			try {
//				esper.registerStatement(stmt.getEPL(), stmt.getName());
//			} catch (EPStatementSyntaxException e) {
//				log.error("Syntax Error in statement: "+e);
//			}
//		}

		//Start injectors
		log.info("Starting Injectors... ");
		for(Map.Entry<String,Injector> entry: injectors.entrySet())  {
			try 	{
				log.info("inj-key: "+entry.getKey());
    				entry.getValue().checkAndRestart();
				}
			catch ( java.lang.Exception ex )
				{
				log.error("Shall not happen: exception in start/stop of an Injector: " + ex) ;
				ex.printStackTrace();
				}
		}

		log.info("Injectors started ");

		esper.sendEvent( new ConfigurationEvent("POST"), "Configuration") ;


		// Add a subscription
		if (configuration.getRDBConfig().contains("rdbconfig")){
			config.Subscription c = new config.Subscription(new UpdateCallback(db, this), "Changing in database!");
			try 	{
			db.subscribe(c);
			log.info("Subscribed to RDB");
			}
			catch ( final config.SystemException ex )
				{
				log.error("Failed to subscribe to RDB: " + ex) ;
				}
		}

		/*// TEST EVENTS	only for debug
		int eventCount = 0;
		while(true) {
			esper.sendEvent(new TestEvent(eventCount));
				eventCount++;
			try {
						Thread.sleep(5000);
				} catch (InterruptedException e) {
						break;
					}
			}*/


		/**
		 * Periodic execution of new IS server check
		 */

		final Runnable newISLookup = new Runnable() {
             		public void run() {
            		//Populating new SmartIS injectors
     			Injectors.checkForNewInjectors(injectors); }
         	};

        /**
         * Schedule action with 10 seconds delay between the termination of
         *  one execution and the commencement of the next.
         *  Initial offset is 20 seconds.
         */
        periodicScheduler.scheduleWithFixedDelay(newISLookup, 20, 10, SECONDS);
        
        /**
		 * Periodic execution for checking IS status
		 */

		 final Runnable IScheck = new Runnable() {
             public void run() {
     			//For every injector do a check and restart
    			for(Map.Entry<String,Injector> entry: injectors.entrySet())  {
				try 	{
    					entry.getValue().checkAndRestart();
					}
				catch ( java.lang.Exception ex )
					{
					log.error("Shall not happen: exception in start/stop of an Injector: " + ex) ;
					ex.printStackTrace();
					}
    			}
             }
         };

        /**
         * Schedule action with 10 seconds delay between the termination of
         *  one execution and the commencement of the next.
         */
        periodicScheduler.scheduleWithFixedDelay(IScheck, 0, 10, SECONDS);

		log.info("Waiting forever...");

		try { Thread.sleep(Long.MAX_VALUE); }
		catch ( final InterruptedException ex ) {} ;

		log.info("Forever finished...");

//		while (true) {
//
//			//Populating new SmartIS injectors
//			Injectors.checkForNewInjectors(injectors);
//
//			//For every injector do a check and restart
//			for(Map.Entry<String,Injector> entry: injectors.entrySet())  {
//				//log.info("Starting Injector: "+entry.getKey());
//				//pool.execute(entry.getValue());
//				entry.getValue().checkAndRestart();
//			}
//
//			try {
//				java.lang.Thread.sleep(10*1000) ;
//			} catch ( java.lang.InterruptedException ex ) {}
//		}
//


	} // start()

	public void shutdown() {

		log.info("Shutting down the engine...");
		for(Map.Entry<String,Injector> injector: injectors.entrySet())  {
			log.info("Stopping Injector: "+injector.getKey());
			injector.getValue().stop(true);
		}

		//Shutting down gracefully the CORBA callback thread pool
		injectorCallbackPool.shutdown();

		//Stopping periodic thread pool
		periodicScheduler.shutdown();

		// close database connections
		ConfigReader.shutdown() ;

		//Destroy the engine thread pools used for inbound and outbound processing
		CEPProvider.getInstance().stop();
		log.info("Shutdown completed");
	}

	public String getRootWriter() {
		return rootWriter;
	}

	public void sendEventNonBlocking(final Object event, final String type) {

		//Deferred send event
		Runnable task = new Runnable() {

			@Override
			public void run() {			
				readLock();
				try {
					esper.sendEvent(event, type);
				} catch (Exception a) {
					log.error("Very bad! Unexpected exception thrown "+ a);
				} finally {	
					readUnlock();
				}	
			}
		};
		try {
			injectorCallbackPool.execute(task);
		} catch (RejectedExecutionException e) {
			log.error("Error submitting task: "+e);
			e.printStackTrace();
		} catch (NullPointerException ex) {
			log.error("Error submitting task: "+ex);
			ex.printStackTrace();
		} catch (Exception a) {
			log.error("Very bad! Unexpected exception thrown "+ a);
		}

	}
	public void addInjector(String name, Injector inj){
		injectors.put(name, inj);
	}

	/**
	 * A shutdown hook is simply an initialized but
	 * unstarted thread that will be started when the JVM begins
	 * its shutdown sequence.
	 */
	private class ShutdownHook extends Thread {
	    public void run() {
	    	AALEngine.getInstance().shutdown();
	    }
	}


}
