package ch.cern.tdaq.cassandra.config;

import java.util.Map;

import daq.EsperUtils.TypedObject;

public class FSUsageStatistics {
	public Map<String, TypedObject> getUsage() {
		return usage;
	}

	public void setUsage(Map<String, TypedObject> usage) {
		this.usage = usage;
	}

	private Map<String, TypedObject> usage;

	public FSUsageStatistics(Map<String, TypedObject> usage) {
		super();
		this.usage = usage;
	}
	
}
