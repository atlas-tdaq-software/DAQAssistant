package ch.cern.tdaq.cassandra.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import daq.EsperUtils.TypedObject;

public class Utils {

	private static final Log log = LogFactory.getLog(Utils.class);

	
/**
 * TODO
 * Understand how this can be done using a Map event type like the one returned by the commented code.
 * @param usage
 * @return
 */
	
//	public static Map parseDisksUsageFromISPMG(String usage ) {
//		String[] sa = usage.split("\\*");
//		//Map<String, Object> result = new HashMap<Strding, Object>();
//		Map event = new HashMap();
//		for(String s:sa){
//			if(s.contains(":")) {
//				String[] tmp = s.split(":");
//				if(tmp.length>1)
//					System.out.println("name:"+tmp[0].replaceAll("/", "").replaceAll(" ", "")
//							+",value:"+new Long(tmp[1].replaceAll("%", "").replaceAll(" ", ""))+":\n");
//					event.put(tmp[0].replaceAll("/", "").replaceAll(" ", ""), new Long(tmp[1].replaceAll("%", "").replaceAll(" ", "")).longValue());
//			}
//		}
//		return event;
//	}

	public static FSUsageStatistics parseFSUsageFromISPMG(String usage ) {
		//log.debug("Parsing the string: "+ usage);
		Map<String, TypedObject> map = new HashMap<String, TypedObject>();
		String[] sa = usage.split("\\*");
		for(String s:sa){
			if(s.contains(":")) {
				String[] tmp = s.split(":");
				if(tmp.length>1) {
					map.put(tmp[0].replaceAll("/", "").replaceAll(" ", ""), new TypedObject(new Long(tmp[1].replaceAll("%", "").replaceAll(" ", ""))));
				}
			}
		}
		return new FSUsageStatistics(map);
	}
}
