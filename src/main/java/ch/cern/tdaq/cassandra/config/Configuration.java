package ch.cern.tdaq.cassandra.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.ArrayList ;
import java.util.List ;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



/**
 * This class is part of the prototype project 
 * to manage complex event processing in the online 
 * system.
 * @author lmagnoni
 *
 */
public class Configuration {
	
	private static final Log log = LogFactory.getLog(Configuration.class);
	
	//Singleton class that make available at run time the
	//properties read from the configuration file.
	//The configuration file name can be passed by command line parameter
	
	private final String filename;
	private final String homedirectory;
	private final String configdirectory;
	private static Configuration instance = null;
	private Properties properties =  null;

	
	private Configuration() {
		this("");
	}
	private Configuration(String directory) {
		if(directory.equals(""))
			this.homedirectory=System.getProperty("user.dir"); //Get homedirectory for INSTALLED/share/data/DAQassistant
		else
			this.homedirectory = directory;
		
		log.debug("setting up configuration with homedirectory:"+this.homedirectory);
		
		configdirectory = homedirectory +"/etc";
		
		filename = "cassandra.properties";
		
		properties = new Properties();
		try {
			log.debug("Looking for configuration files in:"+this.configdirectory);
			log.debug("Opening file " + this.configdirectory+"/"+filename );
	        properties.load(new FileInputStream(this.configdirectory+"/"+filename));
	    } catch (IOException e) {
	    	log.error("Unable to read configuration files "+filename+"! Exit...");
	    	System.exit(1);
	    	
	    }
	}
	
	public static Configuration getInstance() {
		if(instance==null)
			instance = new Configuration();
		return instance;
	}
	
	public Properties getProperties() {
		return properties;
	}
	
	public String getConfigDirectory() {
		return this.configdirectory;
	}
	
	/**
	 * Directory to store the results from file listener
	 * 
	 * @return Name of the homedirectory
	 */
	public String getResultDirectory() {
		return getStringProperties("query.directory","data");
	}
	
	public boolean getFileOverwrite() {
		return getBooleanProperties("output.overwrite", true);
	}

	/**
	 * Rewrite the domain, or to create a copy of a listener with anpther domain, e.g.
	 * directives.domain.redirection = AAl.TDAQ.ShiftLeader=AAL.TDAQ.SLDQ,AAL.TDAQ.ShiftLeader ; AAL.TDAQ.Trigger=AL.TDAQ.RunControl

	*/
	public List<String> getDomainRedirections(final String domain) {
		List<String> ret = new ArrayList<String>() ;
		String redir = getStringProperties("directives.domain.redirection", "") ;
		log.debug("Redirection: " + redir);
		for ( String aredir: redir.split(";") ) {
			log.debug("Redirection: " + aredir);
			if ( aredir.trim().startsWith(domain + "=") ) {
				log.debug("Redirecting domain " + domain);
				for ( String item: aredir.substring(aredir.indexOf('=')+1).split(",") ) {
					log.debug("domain " + domain + " redirected to " + item.trim());
					ret.add(item.trim()) ;
				}
			}
		}
		if ( ret.isEmpty() ) {
			log.debug("domain " + domain + " not redirected");
			ret.add(domain) ;
		}
		return ret ;
	}


	public String getRDBConfig() {
		return getStringProperties("server", "");
	}	

	public String getEmailFrom() {
		return getStringProperties("email.from", "atlas-tdaq-sa-alerts@cern.ch");
	}	

	private boolean getBooleanProperties(String key, boolean defaultValue) {
		if(properties.containsKey(key)) {
			int result  = new Integer(properties.getProperty(key));
			if (result == 0)
				return false;
			else 
				return true;
		} else {
			//Default value
			return defaultValue;
		}
		
	}
	
	
	private int getIntProperties(String key, int defaultValue) {
		if(properties.containsKey(key)) {
			return new Integer(properties.getProperty(key));
		} else {
			//Default value
			return defaultValue;
		}
		
	}
	
	private long getLongProperties(String key, int defaultValue) {
		if(properties.containsKey(key)) {
			return new Long(properties.getProperty(key));
		} else {
			//Default value
			return defaultValue;
		}
		
	}
	
	private String getStringProperties(String key, String defaultValue) {
		if(properties.containsKey(key)) {
			return new String(properties.getProperty(key));
		} else {
			//Default value
			return defaultValue;
		}
		
	}

	private String[] getStringArray(String key, String separator, String defaultValue) {
		String tmp = getStringProperties(key, defaultValue);
		return tmp.replaceAll(" ", "").split(separator);
	}
	
	private Date getDateProperties(String key, Date defaultValue) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		if(properties.containsKey(key)) {
			try {
				
				return formatter.parse(properties.getProperty(key));
				
			} catch (ParseException e) {
				System.err.println("Invalid StartDate format!");
				e.printStackTrace();
				return null;
				
			}
		} else
			return defaultValue;
		
	}

	public int getHoursPerRun() {
		return getIntProperties("db.hoursperrun", 72);
	}
	
	public String getNetlogDBUsername() {
                return getStringProperties("netlog.db.username", "ronly");
        }
        public String getNetlogDBPassword() {
                return getStringProperties("netlog.db.password", "coffee");
        }

        public String getNetlogDBURL() {
                return getStringProperties("netlog.db.URL", "pc-tdq-net-mon-03");
        }

	public String getNetlogDBTestTable() {
                return getStringProperties("netlog.db.testtable", "netlog_netlog");
        }

	public int getISThreads() {
		return getIntProperties("ispool.threads.fixed", 50);
	}
	public int getMRSThreads() {
		return getIntProperties("mrspool.threads.fixed", 50);
	}
	public String getJMSBrokerURL() {
		return getStringProperties("jms.brokerURL", "tcp://localhost:61616");
	}
	public long getRSSTTL() {
		//TTL in secons
		return getLongProperties("jms.rss.ttl", 604800);
	}
	public String getEsperConfigurationFile() {
		return homedirectory + '/' + getStringProperties("esper.configuration", "etc/esper.xml");
	}
	public int getCallbackThreads() {
		return getIntProperties("callbackpool.threads.fixed", 100);
	}
	public String getDirectiveWriterMask() {
		return getStringProperties("directives.writer.mask", "");
	}
	
	public boolean getReplayMode() {
		return getStringProperties("replay.mode", "").toLowerCase().equals("true");
	}

	public String getDB_URL() {
		return getStringProperties("oracle.db.URL", "");
	}
	public String getDB_USER() {
		return getStringProperties("oracle.db.user", "");
	}

	public String getDB_PASSWORD() {
		return getStringProperties("oracle.db.password", "");
	}

	public String getCHIPActionPartition() {
		return getStringProperties("chip.action.partition", "ATLAS");
	}

	public String getCHIPActionInfoName() {
		return getStringProperties("chip.action.info_name", "RunCtrlStatistics.CHIPAction");
	}

}


