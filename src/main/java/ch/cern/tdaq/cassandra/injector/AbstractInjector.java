package ch.cern.tdaq.cassandra.injector;

import ipc.InvalidObjectException;
import ipc.InvalidPartitionException;
import ipc.Partition;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.management.ManagementFactory ;
import java.lang.management.ThreadInfo ;
 
/**
 * Injector abstract class, includes also common "isRunning" and "isServerAlive" functionality based on IPC lookup checks
 * more advanced "isServerAlive" may be implemented in subclasses 
 *  
 * @author lmagnoni
 *
 */
public abstract class AbstractInjector implements Injector {
	
	private final String name;	// internal name or rather an ID, provided upon construction
	private final String ipc_name;  // name as registered in IPC, needed for lookups
	private final String ipc_domain ;   // IPC "domain", seen in ipc_ls, derived from IDL interface name, needed for lookups
	private final Partition ipc_partition ; // IPC partition name, needed for lookups 
	private int start_time ; // seconds from epoch
	protected boolean ISRUNNING ;	    // set to true when injectors starts successfully, to false when stopped
	
	private static final Log log = LogFactory.getLog(AbstractInjector.class.getName());

	public AbstractInjector(String name, String partition_name, String ipc_domain) {
		this(name, partition_name, ipc_domain, name) ;
		}
	
	public AbstractInjector(String name, String partition_name, String ipc_domain, String ipc_name ) {
		log.debug("Creating Injector " + partition_name + "@" + ipc_domain + "@" + ipc_name);
		this.ISRUNNING = false ;
		this.name = name ;
		this.ipc_domain = ipc_domain ;
		this.ipc_name = ipc_name ;
		this.ipc_partition = new Partition(partition_name) ;
		this.start_time = 0 ;
	}
	
	protected Partition getPartition() {
		return ipc_partition;
	}
	
	public String getName() {
		return name;
	}

	private String getIpcName() {
		return getPartition().getName() + "@" + ipc_name;
	}
	
	@Override
	public ipc.servant getIpcObject() {
		try
	            {
	            log.debug("Getting IPC servant " + ipc_partition.getName() + "@" + ipc_domain + "@" + ipc_name );
	            return (ipc.servant)getPartition().lookup(ipc_domain, ipc_name) ;
	            }
                catch (final InvalidPartitionException | InvalidObjectException e )
	            {
		    log.debug("IPC servant " + ipc_partition.getName() + "@" + ipc_domain + "@" + ipc_name + " not found in IPC");
		    return null ;
	            } 
	}

	@Override
	public boolean isRunning() {
		return ISRUNNING;
	}

	@Override
	public ServerState isServerAlive() {
		log.trace("Checking isServerAlive for " +  getIpcName());
		
		if(!getPartition().isValid())
			{
			log.trace("Partition for server " + getIpcName() + " is not up." );
			return ServerState.DOWN ;
			}

		ipc.servant servant = getIpcObject() ;
		if ( servant == null )
			{
			log.trace("Servant " + getIpcName() + " is not running." ) ;
			return ServerState.DOWN ;
			}

		ipc.servantPackage.ApplicationContext context ;
		try	{
			context = servant.app_context() ; // remote call
			}
		catch	( org.omg.CORBA.NO_RESOURCES ex )
			{
			log.fatal("No resources left for CORBA, will exit AAL: " + ex ) ;

			for ( ThreadInfo info : ManagementFactory.getThreadMXBean().dumpAllThreads(true, true) ) {
				{
				System.err.println("################################") ;
				System.err.println(info.toString()) ;
				//for ( StackTraceElement el: info.getStackTrace() ) 
				//	{ System.err.println(el) ; }
				}			
			}
				
			System.exit(666) ; // goodbye, I need to be restarted!
			return ServerState.DOWN; // to calm javac, otherwise "variable ipc_info might not have been initialized"
			}
		catch 	( java.lang.RuntimeException ex )
			{
			log.info("Failed in getting info for Servant " + getIpcName() + ": " + ex ) ;
			return ServerState.DOWN;	
			}

		log.debug("Servant " + getIpcName() + ": start_time:" + context.time + " PID:" + context.pid) ; 

		if ( start_time == 0 )
			{
			log.debug("Servant " + getIpcName() + " checked first time, timestamp stored.") ; 
			start_time = context.time ;
			}

		if ( context.time != start_time )
			{
			log.debug("Servant " + getIpcName() + " was restarted since last check, resubscribe needed.") ; 
			start_time = context.time ;
			return ServerState.EXPIRED ;
			}

		log.debug("Servant " + getIpcName() + " is alive and active." );
		return ServerState.ACTIVE ;		
	}	
	
	@Override
	final public void checkAndRestart() {
		ServerState serverAlive = this.isServerAlive();
		if( !isRunning() && (serverAlive == ServerState.ACTIVE || serverAlive == ServerState.EXPIRED) )
			{
			log.trace("Injector " + getName() + " is not running but it should, so we start it..." );
			this.start();
			}
		else if( isRunning() && (serverAlive == ServerState.EXPIRED) )
			{
			log.trace("Injector " + getName() + " is running but expired, so we stop/start it..." );
			this.stop(true) ;
			this.start();
			}
		else if ( isRunning() && (serverAlive == ServerState.DOWN) ) {
			log.trace("Injector " + getName() + " is running but it's server is down, so we STOP injector..." );
			// ? Two way to stop the injector, depending on the status of the server
			// this is executed only when serverAlive == false
			this.stop(false);
		}
		else
			{
			log.trace("Nothing to be done for Injector " + getName());
			}	
	}

}
