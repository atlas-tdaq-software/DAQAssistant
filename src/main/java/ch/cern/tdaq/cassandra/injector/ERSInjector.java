package ch.cern.tdaq.cassandra.injector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ers.BadStreamConfiguration;
import ers.ReceiverNotFound;

public class ERSInjector extends AbstractInjector {
	
	private final String partition_name;

	private static final String ipc_domain_mts = 	"is/repository" ;
	private static final String ipc_name_mts = 	"MTS" ;
	private static final String subscription = 	"*" ;
//	private static final Class<is.repository>  ipc_class = 	is.repository.class ;

	ERSCallback receiver ;

	private static final Log log = LogFactory.getLog(ERSInjector.class.getName());

	public ERSInjector(String name, String par, String exp) {
		super(name, par, ipc_domain_mts, ipc_name_mts);
		partition_name = par;
		receiver = new ERSCallback(partition_name) ;
	}
	
	@Override
	public void start() { 
		log.trace("Subscribing to MTS in partition" + partition_name);
		
		try     {
	            ers.StreamManager.instance().add_receiver(receiver, "mts", partition_name, subscription) ;
	            log.info("Subscribed to MTS in partition " + partition_name);
	            ISRUNNING = true ;	
	            }
                catch (final BadStreamConfiguration ex)
	            {
	            log.error("NOT subscribed to MTS in partition:" + partition_name + ex.getCause());
	            // get more concrete failure cause
	            try {
					throw ex.getCause() ; // underlying (mts) stream exception: mts.BadExpression,  mts.PublicationFailure
	            	}
	            	catch ( mts.BadExpression mex )
        			{
        			log.fatal("MTS subscription BadExpression: " + mex.getCause()) ;
        			// throw mex ; // can not continue, hardly be here since expression is "*"
        			}
	            	catch ( mts.PublicationFailure pex ) // IPC/IS problems - partition is not up
        			{
        			log.debug("Failed to publish in MTS IS server: " + pex.getCause()) ;
        			}
	            	catch ( Throwable pex )
        			{
        			System.err.println("Unknown/unexpected Cause: " + pex + ": " + pex.getCause()) ;
        			// throw pex ; // can not continue, hardly be here since expression is "*"
   					}

	            ISRUNNING = false;		
	            }
	}
	
	@Override
	public void stop(boolean serverisrunning) {
		if (serverisrunning == false)
			{ log.info("Trying to unsubscribe while there is no running MTS server") ; }
			
		try
	            {
	            ers.StreamManager.instance().remove_receiver(receiver) ;
	            log.debug("Unsubscribed to MTS in partition:" + partition_name) ;
	            }
                catch (ReceiverNotFound e)
	            {
	            log.error(e) ;
	            }
				
		ISRUNNING = false;
		log.debug("Stopped Injector: "+super.getName());
	}
}
