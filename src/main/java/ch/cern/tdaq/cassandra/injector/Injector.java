package ch.cern.tdaq.cassandra.injector;

/**
 * Injector interface
 * 
 * @author lmagnoni akazarov
 *
 */
public interface Injector {
	
	// ACTIVE: UP and subscribed
	// DOWN: not published in IPC or no IPC partition
	// EXPIRED: UP but restarted and subscription is expired: injector needs to be restarted
	public enum ServerState { ACTIVE, DOWN, EXPIRED } ; 

	public abstract void start() ;
	public abstract void stop(boolean serverStatus);
	public abstract void checkAndRestart();
	public abstract boolean isRunning();
	public abstract ServerState isServerAlive();
	public abstract ipc.servant getIpcObject();
}
