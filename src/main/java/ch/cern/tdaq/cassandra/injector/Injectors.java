package ch.cern.tdaq.cassandra.injector;

import ipc.InvalidPartitionException;
import ipc.Partition;
import is.ServerIterator;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.cern.tdaq.cassandra.AALEngine;
import ch.cern.tdaq.cassandra.config.Configuration;

/**
 * Utility class to load Injectors from configuration
 * 
 * @author lmagnoni
 *
 */
public class Injectors {

	private static final Log log = LogFactory.getLog(Injectors.class);

	private Injectors() {
		//Private constructor just to inhibit public one 
	}
	
	public static Map<String, Injector> parseISInjectorFromConfig() {
		log.debug("Loading injectors from configuration...");

		Configuration config = Configuration.getInstance();
		Map<String, Injector> injectors = new HashMap<String, Injector>();
		Properties prop = config.getProperties();
		
		//Set up IS injector from configuration
		//Syntax:
		//is.INJNAME.partition
		//is.INJNAME.server
		//[is.INJNAME.inforname]
		//[is.INJNAME.regex]
		
		//Extracting the list of INJECTOR NAMES
		List<String> injectorsNames = new LinkedList<String>();
		for (Enumeration<Object> e = prop.keys() ; e.hasMoreElements() ;) {
			String name = (String)e.nextElement();
			if(name.contains(".is.")) {
				String[] tokens = name.split("\\.");
				if(tokens.length>=2) {
					if(injectorsNames.indexOf(tokens[0])==-1)
						injectorsNames.add(tokens[0]);
				}
			}
		}
		
		
		//For every AbstractInjector names, look for mandatory and additional informations
		for(String name: injectorsNames) {
			String partition =null;
			String server = null;
			String infoname = null;
			String regex = null;

			if(prop.containsKey(name+".disabled")) {
				log.info("Injector "+name+" is disabled in configuration");
				continue;
			}
			
			if(prop.containsKey(name+".is.partition"))
				partition = prop.getProperty(name+".is.partition");
			else {
				log.error("Invalid injector "+name+" specified. Missing partition.");
				continue;
			}
			
			if(prop.containsKey(name+".is.server"))
				server = prop.getProperty(name+".is.server");
			else {
				log.error("Invalid injector "+name+" specified. Missing server.");
				continue;
			}
			
			if(prop.containsKey(name+".is.infoname"))
				infoname = prop.getProperty(name+".is.infoname");
			
			if(prop.containsKey(name+".is.regex"))
				regex = prop.getProperty(name+".is.regex");
			
			//Creating injector
			if(server.contains(",")) {
				String sa[] = server.replaceAll(" ", "").split(",");
				for(int i=0;i<sa.length;i++) {
					ISInjector injector = new ISInjector(partition+"."+sa[i]+regex, partition, sa[i], infoname, regex);
					injectors.put(injector.getName(), injector);
					log.debug("Injector " + injector.getName() + " subscribed to "+partition+"."+sa[i]+regex+" created" );
				}
			} else  {
				ISInjector injector = new ISInjector(partition+"."+server+regex, partition, server, infoname, regex);
				injectors.put(injector.getName(), injector);
				log.debug("Injector " + injector.getName() + " subscribed to "+partition+"."+server+regex+" created");
			}		
		}
		return injectors;
	}
		
	public static void checkForNewInjectors(Map<String, Injector> runningInjectors) {
		log.trace("Loading  smart injectors from configuration...");

		Configuration config = Configuration.getInstance();
		Properties prop = config.getProperties();
		//Set up IS injector from configuration
		//Syntax:
		//is.INJNAME.partition
		//is.INJNAME.server
		//[is.INJNAME.inforname]
		//[is.INJNAME.regex]
		
		//Extracting the list of INJECTOR NAMES
		List<String> injectorsNames = new LinkedList<String>();
		for (Enumeration<Object> e = prop.keys() ; e.hasMoreElements() ;) {
			String name = (String)e.nextElement();
			if(name.contains(".smartis.")) {
				String[] tokens = name.split("\\.");
				if(tokens.length>=2) {
					if(injectorsNames.indexOf(tokens[0])==-1)
						injectorsNames.add(tokens[0]);
				}
			}
		}
		
		// TODO make it global?
		Map<String, ServerIterator> ISiterators = new HashMap<String, ServerIterator>();
		
		//For every AbstractInjector names, look for mandatory and additional informations
		for(String name: injectorsNames) {
			String partition =null;
			String serverreg = null;
			String infoname = null;
			String regex = null;
			
			if(prop.containsKey(name+".smartis.partition"))
				partition = prop.getProperty(name+".smartis.partition");
			else {
				log.error("Invalid injector "+name+" specified. Missing partition.");
				continue;
			}
			
			if(prop.containsKey(name+".smartis.server"))
				serverreg = prop.getProperty(name+".smartis.server");
			else {
				log.error("Invalid injector "+name+" specified. Missing server.");
				continue;
			}
			
			
			if(prop.containsKey(name+".smartis.infoname"))
				infoname = prop.getProperty(name+".smartis.infoname");
			
			if(prop.containsKey(name+".smartis.regex"))
				regex = prop.getProperty(name+".smartis.regex");
			
		
			//Creating the IS servers list complaint to user specified regexp
			Pattern p = Pattern.compile(serverreg);
			log.trace("Creating pattern for : " +partition+", regexp: "+serverreg);
			
			//Check if partition is alive and object exists
			Partition ipc_partition = new Partition(partition);
			if(ipc_partition.isValid()) {
				log.trace("Getting IS server iterator for :" +serverreg);
				/**
				 * Reuse the same IS iterator. Creation time is not negligible, apparently...
				 */
				ServerIterator isit;
				if (ISiterators.containsKey(partition)) {
					isit = ISiterators.get(partition);
					isit.reset();
					//Build regexp with user specified string
					while (isit.hasMoreElements()) {
						String servername = isit.nextServerName();
						if ((p.matcher(servername).matches()) && (!runningInjectors.containsKey(partition+"."+servername))) {
							log.trace("New server found comparing '"+servername+"' with: '"+serverreg+"'");
							ISInjector injector = new ISInjector(partition+"."+servername, partition, servername, infoname, regex);
							AALEngine.getInstance().addInjector(partition+"."+servername, injector);
							log.trace("Injector "+partition+"."+servername+" added to main injectors list.");
						}
					}
				} else {
					try
	                                    {
	                                    isit = new ServerIterator(new Partition(partition));
	                                    ISiterators.put(partition, isit);
	                                    }
                                        catch (InvalidPartitionException e)
	                                    {
	                                    log.error("Creating IS iterator for " + serverreg + ": " + e) ;
	                                    }	
				}
			}
		}		
	}

	public static Map<String, Injector> parseSmartISInjector() {
		log.trace("Loading  smart injectors from configuration...");

		Configuration config = Configuration.getInstance();
		Map<String, Injector> injectors = new HashMap<String, Injector>();
		Properties prop = config.getProperties();
		
		//Set up IS injector from configuration
		//Syntax:
		//is.INJNAME.partition
		//is.INJNAME.server
		//[is.INJNAME.inforname]
		//[is.INJNAME.regex]
		
		//Extracting the list of INJECTOR NAMES
		List<String> injectorsNames = new ArrayList<String>();
		for (Enumeration<Object> e = prop.keys() ; e.hasMoreElements() ;) {
			String name = (String)e.nextElement();
			if(name.contains(".smartis.")) {
				String[] tokens = name.split("\\.");
				if(tokens.length>=2) {
					if(injectorsNames.indexOf(tokens[0])==-1)
						injectorsNames.add(tokens[0]);
				}
			}
		}
		
		// TODO make it global?
		Map<String, ServerIterator> ISiterators = new HashMap<String, ServerIterator>();
		
		//For every AbstractInjector names, look for mandatory and additional informations
		for(String name: injectorsNames) {
			String partition =null;
			String serverreg = null;
			String infoname = null;
			String regex = null;
			
			if(prop.containsKey(name+".smartis.partition"))
				partition = prop.getProperty(name+".smartis.partition");
			else {
				log.error("Invalid injector "+name+" specified. Missing partition.");
				continue;
			}
			
			if(prop.containsKey(name+".smartis.server"))
				serverreg = prop.getProperty(name+".smartis.server");
			else {
				log.error("Invalid injector "+name+" specified. Missing server.");
				continue;
			}
			
			if(prop.containsKey(name+".smartis.infoname"))
				infoname = prop.getProperty(name+".smartis.infoname");
			
			if(prop.containsKey(name+".smartis.regex"))
				regex = prop.getProperty(name+".smartis.regex");
			
			//Creating injector
			//Creating the IS servers list compliant to user specified regexp
			Pattern p = Pattern.compile(serverreg);
			log.trace("Creating patter for : " +partition+", regexp: "+serverreg);
			
			/**
			 * Reuse the same IS iterator. Creation time is not negligible, apparently...
			 */
			ServerIterator isit;
			if (ISiterators.containsKey(partition)) {
				isit = ISiterators.get(partition);
				isit.reset();
			} else {
				try {
	                            isit = new ServerIterator(new Partition(partition));
	    				//Build regexp with user specified string
	    			    while (isit.hasMoreElements()) {
	    				String servername = isit.nextServerName();
	    				if (p.matcher(servername).matches())  {
	    					log.trace("Match found comparing '"+servername+"' with: '"+serverreg+"'");
	    					ISInjector injector = new ISInjector(partition+"."+servername, partition, servername, infoname, regex);
	    					injectors.put(partition+"."+servername, injector);
	    					log.trace("Injector "+partition+"."+servername+" created.");
	    				}
	    			    ISiterators.put(partition, isit);	
	    			    }	
	                        }
                                catch (InvalidPartitionException e)
	                            {
	                            ers.Logger.error(e) ;
	                            }
			}
		}
		return injectors;
	}
	
	public static Map<String, Injector> parseERSInjectorFromConfig() {
		log.debug("Loading injectors from configuration...");
		Configuration config = Configuration.getInstance();
		Map<String, Injector> injectors = new HashMap<String, Injector>();
		Properties prop = config.getProperties();

		//Set up MRS injector from configuration
		//Syntax:
		//mrs.INJNAME.partition
		//[mrs.INJNAME.regex]

		//Extracting the list of INJECTOR NAMES
		List<String> injectorsNames = new ArrayList<String>();
		
		for (Enumeration<Object> e = prop.keys() ; e.hasMoreElements() ;) {
			String name = (String)e.nextElement();
			if(name.contains(".ers.")) {
				String[] tokens = name.split("\\.");
				if(tokens.length>=2) {
					if(injectorsNames.indexOf(tokens[0])==-1)
						injectorsNames.add(tokens[0]);
				}
			}
		}


		//For every AbstractInjector names, look for mandatory and additional informations
		for(String name: injectorsNames) {
			String partition =null;
			String regex = null;

			if(prop.containsKey(name+".ers.partition"))
				partition = prop.getProperty(name+".ers.partition");
			else {
				log.error("Invalid injector "+name+" specified. Missing partition.");
				continue;
			}
			
			if(prop.containsKey(name+".ers.regex"))
				regex = prop.getProperty(name+".ers.regex");
			else {
				log.error("Invalid injector "+name+" specified. Missing partition.");
				continue;
			}

			//TODO add inforname and regex

			//Creating injector
			ERSInjector injector = new ERSInjector(name, partition, regex);
			injectors.put(name, injector);
			log.debug("AbstractInjector "+name+" created.");
		}
		
		return injectors;
	}
	
	
}
