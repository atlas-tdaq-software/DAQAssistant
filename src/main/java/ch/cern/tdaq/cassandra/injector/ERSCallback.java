package ch.cern.tdaq.cassandra.injector;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.cern.tdaq.cassandra.AALEngine;
import daq.EsperUtils.Events;
import daq.EsperUtils.ERSEvent;

public class ERSCallback implements  ers.IssueReceiver { 
	
	private static final Log log = LogFactory.getLog(ERSCallback.class);
	private final String partition;
	
	public ERSCallback(String partition) {
		this.partition = partition;
	}
	
	@Override
	public void receive(ers.Issue issue) {
		
		//Debug to remove
		log.trace(issue.context().application_name() + " Received: " + issue.getMessage() );

		for ( ERSEvent mess: Events.createERSEvent(issue, partition) )
				AALEngine.getInstance().sendEventNonBlocking(mess, "Message");
	}
}
