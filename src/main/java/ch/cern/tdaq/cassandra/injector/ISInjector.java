package ch.cern.tdaq.cassandra.injector;

import is.Repository;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 *
 * Injector class for the IS server.
 * 
 * 
 * 
 * @author lmagnoni
 *
 */

public class ISInjector extends AbstractInjector {
	
	private static final Log log = LogFactory.getLog(ISInjector.class.getName());
	private static final String ipc_domain_is = 	"is/repository" ;
//	private static final Class<is.repository>  ipc_class = 	is.repository.class ;

	private final String server;
	private final String infoname;
	private final String regex;
	private final is.Criteria criteria ;
	private final Repository is_repository ;
	
	// private boolean pending_unsubscribe = false;
	
	/*
	 *  Using builder for more optional parameter
	 */
	
	/**
	 * 
	 * @param server
	 * @param infoname
	 * @param criteria 
	 */
	public ISInjector(final String name, final String partition, final String server, final String infoname, final String regex)
	{
		super(name, partition, ipc_domain_is, server);
		this.server = server;
		this.infoname = infoname;
		//TODO Bettter sanitizing...
		this.regex = regex.replaceAll(" ", "");
		this.criteria = new is.Criteria(java.util.regex.Pattern.compile(this.regex)) ; 
		is_repository = new Repository( getPartition() );
		
		// Criteria as defined here in the IS user manual section:
		//http://atlas-tdaq-monitoring.web.cern.ch/atlas-tdaq-monitoring/IS/doc/userguide/html/is-usersguide-16.html#pgfId-808821		
	}


	@Override
	public void start() { 
		log.info("Starting IS Injector: "+super.getName());

		try {
			if( infoname!=null && !infoname.equals("") ) {
				//Subscribing to IS server with specific information name		
				is_repository.subscribe(server+"."+infoname, new ISCallback(getPartition(), server));
				log.debug("Subscribed to IS: " +getPartition().getName()+"."+server+"."+infoname);
			
			}  else if (regex!=null && !regex.equals("")) {
				is_repository.subscribe(server, criteria,  new ISCallback(getPartition(), server));
				log.debug("Subscribed to IS: " +getPartition().getName()+"."+server+" with criteria: '"+regex+"'");
			}	
		} // try subscribe to IS
		catch ( is.RepositoryNotFoundException | is.InvalidCriteriaException ex ) {
			log.debug("Failed to subscribe to: " +getPartition().getName()+"."+server) ;
			this.ISRUNNING=false;
			return;
		}
		catch ( is.AlreadySubscribedException ex ) {
			log.error("Already subscribed to: " +getPartition().getName()+"."+server) ;
		}
		
		this.ISRUNNING=true;
		log.debug("Started Injector: "+super.getName());
	}

	@Override
	public ServerState isServerAlive() {
		if ( ISRUNNING ) return ServerState.ACTIVE ; // once we have started IS Injector, no need to stop it and resubscribe
		log.debug("IS Injector was not yet started: "+super.getName()) ;
		return super.isServerAlive() ;
	}

	@Override
	public void stop(boolean serverisrunning) {
// shall be only called once at exit from AAL
		log.info("Stopping IS Injector: "+super.getName()+" server status is "+serverisrunning);
		if(serverisrunning) {
			try {
				if(infoname!=null && !infoname.equals("")) {
					is_repository.unsubscribe(server+"."+infoname);
					log.debug("Unsubscribed to: " +getPartition()+"."+server+"."+infoname);
				}  else if (regex!=null && !regex.equals("")) {
					is_repository.unsubscribe(server, criteria);
					log.debug("Unsubscribed to: " +getPartition()+"."+server+" with criteria: "+regex);
				}
			}
			catch ( is.RepositoryNotFoundException ex ) // Server went down
			{ log.trace("Server is down: " + ex); this.ISRUNNING=false ;}
			catch ( is.SubscriptionNotFoundException ex )
			{ log.error("Failed to unsubscribe from IS: " + ex + "\n"); this.ISRUNNING=false;}
		} 

		this.ISRUNNING=false;
		log.info("Stopped Injector: "+super.getName());
	}
}


