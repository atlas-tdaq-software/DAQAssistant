package ch.cern.tdaq.cassandra.injector;

import com.espertech.esper.common.client.EPException;

import ipc.Partition;
import is.InfoEvent;
import is.InfoListener;
import is.InfoSubscribedListener;

import daq.EsperUtils.ISEvent;
import daq.EsperUtils.Events;
import daq.EsperUtils.CepException.InvalidEventException;

import ch.cern.tdaq.cassandra.AALEngine;

public class ISCallback implements InfoListener, InfoSubscribedListener{
	
	private final Partition p;
	private final String server;
	
	public ISCallback(final Partition p, final String server) {
		this.p = p;
		this.server = server;
	}

	@Override
	public void infoCreated(final InfoEvent arg0) {
		infoEventGen(ISEvent.OP.CREATE, arg0);
	}

	// Subscibed gets called upon subscription, so it is just like Created
	@Override
	public void infoSubscribed( InfoEvent arg0 )
	{
		infoEventGen(ISEvent.OP.SUBSCRIBE, arg0);
	}

	@Override
	public void infoDeleted(final InfoEvent arg0) {
		infoEventGen(ISEvent.OP.DELETE, arg0);
	}

	@Override
	public void infoUpdated(final InfoEvent arg0) {
		infoEventGen(ISEvent.OP.UPDATE, arg0);
	}
	
	private void infoEventGen(final ISEvent.OP operation, final InfoEvent arg0) {
		ISEvent event;
		try {
			event = Events.createISEvent(operation, arg0, p, server);
			AALEngine.getInstance().sendEventNonBlocking(event, "ISEvent");
			ers.Logger.debug(2, "New ISEvent created with name: "+arg0.getName()+" type:"+arg0.getType().getName());
		} catch (final InvalidEventException e) {
			ers.Logger.error(e);
		} catch (final EPException ex) {
			ers.Logger.error(ex);
		} catch (final Exception a) {
			System.err.println("Very bad! Unexpected exception thrown") ;
			ers.Logger.error(a) ;
		}
	}
	

}
