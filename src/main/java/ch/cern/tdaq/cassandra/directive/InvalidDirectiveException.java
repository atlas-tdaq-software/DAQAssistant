package ch.cern.tdaq.cassandra.directive;

public class InvalidDirectiveException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 /**
     * Ctor.
     * @param message - error message
     */
    public InvalidDirectiveException(final String message) {
        super(message);
    }

}
