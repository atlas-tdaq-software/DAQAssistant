package ch.cern.tdaq.cassandra.directive;

import ch.cern.tdaq.cassandra.schema.SAInitialStatement;
import ch.cern.tdaq.cassandra.schema.SAInitialStatementBase;
import ch.cern.tdaq.cassandra.schema.SAInitialStatementSet;
import ch.cern.tdaq.cassandra.schema.SAInitialStatementSet_Helper;
import ch.cern.tdaq.cassandra.schema.SAInitialStatement_Helper;
import ch.cern.tdaq.cassandra.schema.SAListener;
import ch.cern.tdaq.cassandra.schema.SAConfig;
import ch.cern.tdaq.cassandra.schema.SAConfig_Helper;
import ch.cern.tdaq.cassandra.schema.SAWriter;
import ch.cern.tdaq.cassandra.schema.SADirective;
import ch.cern.tdaq.cassandra.schema.SADirectiveBase;
import ch.cern.tdaq.cassandra.schema.SADirectiveSet_Helper;
import ch.cern.tdaq.cassandra.schema.SADirective_Helper;
import ch.cern.tdaq.cassandra.schema.SAEmail;
import ch.cern.tdaq.cassandra.schema.SAEmail_Helper;
import ch.cern.tdaq.cassandra.schema.SAErs;
import ch.cern.tdaq.cassandra.schema.SAErs_Helper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;



public class Schema {

	private static final Log log = LogFactory.getLog(Schema.class);

	private static config.Configuration db;
	
	
	public Schema(config.Configuration database) {
		Schema.db = database;
	}
	
		
	public static List<SAInitialStatement> parseInitialStatements() throws config.ConfigException {
		SAConfig original = SAConfig_Helper.get(db, "main");
		SAInitialStatementBase[] initialstatements = original.get_initialstatements();
		List<SAInitialStatement> initialstatementList = new ArrayList<SAInitialStatement>();
		for (SAInitialStatementBase i : initialstatements) {
			if (i.class_name().equals("SAInitialStatement")) {
				SAInitialStatement initstmt = SAInitialStatement_Helper.cast(i);
				initialstatementList.add(initstmt);
			} else
			if (i.class_name().equals("SAInitialStatementSet")) {
				SAInitialStatementSet initstmtSet = SAInitialStatementSet_Helper.cast(i);
				initialstatementList.addAll(getInitialStatement(initstmtSet));
			}
		}
		return initialstatementList;
	}
	
	
	public static List<SAInitialStatement> getInitialStatement(SAInitialStatementSet initset) throws config.ConfigException {
		List<SAInitialStatement> initialstatementpart = new ArrayList<SAInitialStatement>();
		for (SAInitialStatementBase i : initset.get_Contains()) {
			if (i.class_name().equals("SAInitialStatement")) {
				SAInitialStatement initstmt = SAInitialStatement_Helper.cast(i);
				initialstatementpart.add(initstmt);
			} else
			if (i.class_name().equals("SAInitialStatementSet")) {
				SAInitialStatementSet initstmtSubSet = SAInitialStatementSet_Helper.cast(i);
				initialstatementpart.addAll(getInitialStatement(initstmtSubSet));
			}
		}
		return initialstatementpart;		
	}


	public static Map<String, SADirective> parseDirectives() throws config.ConfigException {
		SAConfig original = SAConfig_Helper.get(db, "main"); // TODO check for null
		Map<String, SADirective> directivesMap= new HashMap<String, SADirective>();
		getDirectives(original.get_directives(), directivesMap) ;
		return directivesMap;
	}

	
	public static void getDirectives(SADirectiveBase[] directives, Map<String, SADirective> directivesMap) throws config.ConfigException {
		for (SADirectiveBase i : directives) {
			if (i.class_name().equals("SADirective")) {
				SADirective directive = SADirective_Helper.cast(i);
				directivesMap.put(directive.UID(),directive);
			} else
			if (i.class_name().equals("SADirectiveSet")) {
				getDirectives(SADirectiveSet_Helper.cast(i).get_Contains(), directivesMap);
			}
		}
	}
	
	
	public static List<String> allEsperStatementsFromConfig() throws config.ConfigException {
		
		List<String> statementNames = new ArrayList<String>();
		
		for (SAInitialStatement i : parseInitialStatements()) {
			statementNames.add(i.UID());
		}
		
		for (SADirective d : parseDirectives().values()){
			for (SAInitialStatement initepl : d.get_initialstatements()) {
				statementNames.add(initepl.UID());
			}
			statementNames.add(d.get_epl().UID());
		}

		return statementNames;
	}
		
	public static SAEmail getEmailFromListener(SAListener l) throws config.ConfigException {
		
		SAEmail obj = null;
		for (SAWriter w : l.get_writers()) {
			if (w.class_name().equals("SAEmail")) {
				obj = SAEmail_Helper.get(db, w.UID());
			}
		}
		return obj;
	}
	
	public static SAErs getErsFromListener(SAListener l) throws config.ConfigException {
		
		SAErs obj = null;
		for (SAWriter w : l.get_writers()) {
			if (w.class_name().equals("SAErs")) {
				obj = SAErs_Helper.get(db, w.UID());
			}
		}
		return obj;
	}
	
	
}
