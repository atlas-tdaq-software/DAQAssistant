package ch.cern.tdaq.cassandra.directive;

import ch.cern.tdaq.cassandra.output.HandlerUtils;
import com.espertech.esper.common.client.EventBean;


public class Alert {
	
	private final String domain;
	private final String severity;
	/** 
	 * Brief description of the problem
	 */
	private final String message;
	
	/**
	 * Suggested action 
	 */
	private final String action;
	
	private EventBean[] newEvents;
	//TODO add the old Events string
	private final String name; //Identifier for the specific Alert
	
	public EventBean[] getNewEvents() {
		return newEvents;
	}

	
	public String getDomain() {
		return domain;
	}

	public String getSeverity() {
		return severity;
	}

	public String getName() {
		return name;
	}

	public String getMessage() {
		return message;
	}

	public String getAction() {
		return action;
	}
	
	/**
	 * Builder class. 
	 * To avoid  long list of parameterized constructors and simulates 
	 * named optional parameter, a la Python.
	 * 
	 */
	public static class Builder {
		//Required parameters
		private final String domain;
		private final String severity;
		
		//Optional parameters
		private String name = "";
		private String message = "-";
		private String action = "-";
		private EventBean[] newEvents = null;
		
		public Builder(String domain, String severity) {
			this.domain = domain;
			this.severity = severity;
		}
		
		public Builder name(String name) {
			this.name = name;
			return this;
		}
	
		public Builder message(String m) {
			this.message = m;
			return this;
		}
		
		public Builder action(String a) {
			this.action = a;
			return this;
		}
		
		public Builder newEvents(EventBean[] events) {
			this.newEvents = events;
			return this;
		}
	
		public Alert build() {
			//Default value to protect from null
			if(name==null)
				name="default";

			if (newEvents==null)
				newEvents = new EventBean[0];
			return new Alert(domain, severity, message, action, name, newEvents);
		}
	}
	
	
	private Alert(String domain, String severity, String message, String action, String name, EventBean[] newEvents) {
		super();
		this.domain = domain;
		this.severity = severity;
		this.message = HandlerUtils.parseVariables(message, newEvents);
		this.action = HandlerUtils.parseVariables(action, newEvents);
		this.name = name;
		this.newEvents=newEvents;
	}

}
