package ch.cern.tdaq.cassandra.directive;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.cern.tdaq.cassandra.AALEngine;
import ch.cern.tdaq.cassandra.output.FileResultHandler;
import ch.cern.tdaq.cassandra.output.ERSResultHandler;
import ch.cern.tdaq.cassandra.output.MailResultHandler;
import ch.cern.tdaq.cassandra.output.DBResultHandler;
import ch.cern.tdaq.cassandra.output.OutputHandler;
import ch.cern.tdaq.cassandra.output.CHIPActionHandler;

import com.espertech.esper.runtime.client.EPRuntime;
import com.espertech.esper.runtime.client.EPStatement;
import com.espertech.esper.common.client.EventBean;

import ch.cern.tdaq.cassandra.schema.SAListener;
import ch.cern.tdaq.cassandra.schema.SAWriter;
import ch.cern.tdaq.cassandra.schema.SAFile;
import ch.cern.tdaq.cassandra.schema.SAFile_Helper;
import ch.cern.tdaq.cassandra.schema.SAErs;
import ch.cern.tdaq.cassandra.schema.SAErs_Helper;
import ch.cern.tdaq.cassandra.schema.SAEmail_Helper;
import ch.cern.tdaq.cassandra.schema.CHIPAction;
import ch.cern.tdaq.cassandra.schema.CHIPAction_Helper;
import ch.cern.tdaq.cassandra.schema.SAEmail;
import ch.cern.tdaq.cassandra.directive.Alert;
import ch.cern.tdaq.cassandra.config.Configuration;


public class AlertListener implements com.espertech.esper.runtime.client.UpdateListener {

	private static final Log log = LogFactory.getLog(AlertListener.class);

	private final String listener_name;

	private final String domain;
	private final String message;
	private final String action;
	private final String severity;
	private final boolean eventDetails;
	//	private final boolean verbose;

	private final List<OutputHandler> output_handler;


	public AlertListener(String dirname, String sdomain, String smessage, String saction, String sseverity, boolean seventDetails, List<OutputHandler> output_handler) {

		this.listener_name = dirname;
		this.domain = sdomain;
		this.message = smessage;
		this.severity = sseverity;
		this.action = saction;
		this.eventDetails = seventDetails;
		this.output_handler = output_handler;
		
		}

	public static List<AlertListener> fromListToAlertList(SAListener[] listeners, String dirname) throws config.ConfigException {

		List<AlertListener> alist = new ArrayList<AlertListener>();
			for (SAListener l : listeners){
				List<OutputHandler> out = fromWritersToHandler(l, dirname);
				for ( final String domain: Configuration.getInstance().getDomainRedirections(l.get_domain()) ) {
					if ( domain.equals("null") ) continue ;
					log.info("Alert listener dor domain " + domain + " added") ;
					AlertListener a = new AlertListener(dirname, domain, l.get_message(), l.get_action(), l.get_severity(), l.get_eventDetails(), out);
					//	log.info("New AlertListener! name: "+dirname+", domain: "+l.get_domain()+", message: "+l.get_message());
					alist.add(a);
				}
			}
			return alist;
		}

	public static List<OutputHandler> fromWritersToHandler(SAListener l, String dirname) throws config.ConfigException {

		//The RootWriter can be used to overwrite all the explicit
		//writer. I.e. is useful in a testing instance to force all output
		//to file without touching the real directives file.
		//Getting the writer from directiveslistname
		String rootWriter = AALEngine.getInstance().getRootWriter().replaceAll(" ", "");
		//Parsing rootwriter
		Map<String, String> remapping = new HashMap<String, String>();
		if(!rootWriter.equals("")) {
			if(!rootWriter.contains(",")) {
				rootWriter = rootWriter.concat(",");
			}
			for(String s:rootWriter.split(",")){
				String[] exp = s.split("=");
				remapping.put(exp[0], exp[1]);
			}
		}

		List<OutputHandler> outlist = new ArrayList<OutputHandler>();
		
		
		for (SAWriter w : l.get_writers()) {
			
			String output = w.class_name().replace("SA", "");
			OutputHandler out = null;

			if(remapping.containsKey(output))
				output = remapping.get(output);
			
			if(output.equals("null")) {
				//log.warn("Handler "+output+" for "+dirname+" not created because it's masked.");
				continue;
			}
			
			if (output.equals("File")) {
				SAFile safile = SAFile_Helper.cast(w) ;
				if ( safile != null ) {
					out = new FileResultHandler(dirname + ".out", safile.get_append());
				} else {
					out = new FileResultHandler(dirname + ".out");
				}
			} else
			if (output.equals("Db")) {	
				out = DBResultHandler.INSTANCE;
			} else
			if (output.equals("Email")) {
				SAEmail saemail = SAEmail_Helper.cast(w) ; 
				if (saemail != null) {
					out = new MailResultHandler(saemail, dirname);
				} else {
					log.error("Email addresses not specified in Writer " + w);
				}
			} else
			if (output.equals("Ers")) {
				SAErs saers = SAErs_Helper.cast(w) ;
				if ( saers != null) {
					out = new ERSResultHandler(saers, dirname);
				} else {
					log.error("ERS qualifiers needed!");
				}
			} else
			if (output.equals("CHIPAction")) {
				CHIPAction chipa = CHIPAction_Helper.cast(w) ;
				if ( chipa != null) {
					out = new CHIPActionHandler(chipa);
				} else {
					log.error("Wrong CHIPAction");
				}
			}
			else	{
				log.error("Did not manage to cast Writer " + w.UID() + " to any of the concrete classes!") ;
			}

			if (out != null) outlist.add(out) ;
			
		} // for all writers of this alert
			
		return outlist;

	}

	@Override
	public void update(EventBean[] newEvents, EventBean[] oldEvents, EPStatement stmnt, EPRuntime rt) {
		if(newEvents != null) {

		//	if(verbose)
		//		log.debug(listener_name+": Update with: "+(newEvents==null?"0":newEvents.length)+" new events");
		// ADD VERBOSE IN OKS SCHEMA?

			//Creating generic alert
			Alert.Builder builder = new Alert.Builder(domain, severity);

			//Adding message
			builder = builder.message(message);
			//Adding action
			builder = builder.action(action);

			//Adding events details if needed
			if(eventDetails)
				builder = builder.newEvents(newEvents);

			//setting up alert name
			builder = builder.name(listener_name);

			//Creating alert
			Alert alert = builder.build();

			//Write to all the handler
			for ( OutputHandler oh: output_handler )
				oh.write(alert);

			log.info("UPDATING: "+listener_name + " new events: " + newEvents.length);
	
		} else {
			/**
			 http://esper.espertech.com/release-4.10.0/esper-reference/html/appendix_outputspec.html
			 
			 *  This is the case for empty result reported by EPL statement with output limiting.
			 *  Esper is calling the listener every time the output condition is reached, i.e. every minute,
			 *  even if there are no interesting event.
			 *  This behavior is better explained at the beginning of Appendix A:
			 *  			 *  We don't want to trigger an alert on empty results...
			 */
			
			// log.info("Alert triggered (without events): " + listener_name + " " + message);
		}
	}


}
