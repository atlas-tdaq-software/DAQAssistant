package ch.cern.tdaq.cassandra.reader;

/**
 * This class can be used in EPL statement to retrieve partial configuration information,
 * related to enabled/disabled components in the configuration.
 * isComponentEnabled: It works for classes derived from HW_Object (State attribute) and from Component (disabled algorithm in Partition)
 *
 * Example:
 * select .... from method:ISReader.isComponentEnabled("ATLAS", "Computer", computer) as enabled
 * where enabled
 *
 * @author akazarov
 *
 */

import java.util.concurrent.ConcurrentHashMap;
import java.util.*;
import java.sql.*;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ipc.InvalidObjectException;
import ipc.InvalidPartitionException;
import ipc.Partition;
import config.Configuration;
import config.Change;
import config.Subscription;
import config.SystemException;
import config.NotFoundException;
import daq.EsperUtils.TypedObject;
import ch.cern.tdaq.cassandra.utils.BooleanBean;
import ch.cern.tdaq.cassandra.utils.ComponentStatus;
import ch.cern.tdaq.cassandra.utils.IntegerBeam;

public class ConfigReader
{
	private static final Log log = LogFactory.getLog(ConfigReader.class);

	static boolean static_config = false; 
	final static String DB_DEFAULT_IMPLEMENTATION = "rdbconfig";
	final static String DB_DEFAULT_SERVER_NAME = "RDB";
	final static String DB_DEFAULT_SERVER_NAME_INITIAL = "RDB_INITIAL";
	final static String CLASS_PREFIX = "dal";
	// final static String CLASS_SUFFIX = "_Helper";
	final static TypedObject NoObject = new TypedObject(new Object()) ;

	static class ConfigInfo
	{
		config.Configuration config ;
		int start_time ;
		ConfigInfo( config.Configuration conf, int time) { config = conf; start_time = time ; } 
	}

	static class MyCallback implements config.Callback
	{
		public void process_changes(Change[] changes, java.lang.Object parameter) { 
			log.info("Clear all configurations since we received a callback from RDB");
					
		}
	}

	static ConcurrentHashMap<String, ConfigInfo> loaded_configs = new ConcurrentHashMap<String, ConfigInfo>() ;

	// get RDB start timestamp, to compare
	static int getRDBTimestamp(final String partition_name)
	{
		Partition ipc_partition = new Partition(partition_name) ;

		ipc.servant rdb_servant;
		try
		{
			rdb_servant = ipc_partition.lookup(rdb.cursor.class, DB_DEFAULT_SERVER_NAME);
		}
		catch (InvalidPartitionException | InvalidObjectException e)
		{
			log.info("Partition RDB server not found in IPC. Invalidating configuration for partition " + partition_name);
			log.debug("Exception: " + e);
			return 0 ;
		}

		if ( rdb_servant == null )
		{	
			log.info("Partition RDB server not found in IPC (null). Invalidating configuration for partition " + partition_name);
			return 0 ;
		}

		ipc.servantPackage.ApplicationContext context ;

		try	{
			context = rdb_servant.app_context() ; // remote call, may cause exceptions
		}
		catch 	( java.lang.RuntimeException ex )
		{
			// log.trace("Failed in getting IPC info for RDB server: " + ex ) ;
			log.info("Partition RDB server is not accessible: " + ex + ". Invalidating configuration for partition " + partition_name);
			return 0 ;
		}

		return context.time ;
	}

	// to be called in Engine shutdown hook
	public static void shutdown()
	{
		for ( Map.Entry<String, ConfigInfo> entry : loaded_configs.entrySet() )
		{
			try {
				if ( entry.getValue().config == null ) continue ;	
				entry.getValue().config.unsubscribe(); 
				entry.getValue().config.unload() ; 
			}
			catch ( Exception ex )
			{
				log.error("Failed to unload Configuration for partition " + entry.getKey() + ": " + ex) ;
			}
			log.info("Unloaded Configuration for partition " + entry.getKey()) ;
		}
	}
	
	// singleton-like method for accessing Configuration in a reliable manner
	static synchronized Configuration getConfiguration(final String partition_name) {		
		ConfigInfo config_info = null ;

		if ( loaded_configs.containsKey(partition_name) )
		{
			config_info = loaded_configs.get(partition_name) ;
			if ( config_info.config != null ) // we have some config, but need to check it's alive by accessing RDB ipc servant
			{
				log.debug("Configuration found for partition " + partition_name + ". Check if it's still valid.");
				int rdbtimestamp = getRDBTimestamp(partition_name) ;

				if ( static_config == false && (config_info.start_time != rdbtimestamp || rdbtimestamp == 0)) // RDB restated or not running, we have out-of-date Configuration
				{
					log.info("Invalidating cached configuration for partition " + partition_name);
					try {
						config_info.config.unsubscribe();
						config_info.config.unload();
					}
					catch(final Exception ex) {
						log.error("Failed to unload Configuration for " + partition_name) ;
						ex.printStackTrace() ;
					}
					config_info.config = null ;
					config_info.start_time = rdbtimestamp ; // pass to the new Configuration() step
				}
				else	// finally we validated our Configuration object 
				{
					log.debug("Cached Configuration for partition " + partition_name + " is OK") ;
					return config_info.config ;
				}
			} // else: config_info.config == null, will pass to the new Configuration() step
		}
		else	// accessing first time for this partition
		{
			config_info = new ConfigInfo(null, 0) ;
			loaded_configs.put(partition_name, config_info) ;
			if(static_config) return null;
		}

		// config_info.config == null
		// configuration either not loaded or was invalidated (i.e. partition does not exist or RDB server restarted)
		
		try	{
			config_info.start_time = getRDBTimestamp(partition_name) ;
			if ( config_info.start_time == 0 )
			{
				log.info("RDB for partition " + partition_name + " is not up") ;
				return config_info.config ; // null
			}
			log.info("Creating new Configuration for partition " + partition_name) ;
			config_info.config = new Configuration(DB_DEFAULT_IMPLEMENTATION + ":" + DB_DEFAULT_SERVER_NAME + "@" + partition_name) ;

			dal.Partition partition = dal.Partition_Helper.get(config_info.config, partition_name) ;
			if (partition == null) {
				log.error("No partition with name " + partition_name + " found.") ;
				config_info.config = null;
				return null ;
			}

			dal.SubstituteVariables dalConv = new dal.SubstituteVariables(partition);
			config_info.config.register_converter(dalConv);
            		// Subscribe to changes
            		Subscription s = new Subscription( new MyCallback(), null ) ;
			config_info.config.subscribe(s) ;
		}
		catch ( final config.ConfigException ex )
		{	
			log.error("Problem creating new partition Configuration: " + ex) ;
			config_info.config = null ;
		}

		return config_info.config ;
	}
	
	// Special for sareplay
	public static synchronized void injectStaticConfiguration(String partitionName, config.Configuration conf) {
		log.info("Cache static configuration for partition " + partitionName);
		ConfigInfo config_inf = new ConfigInfo(conf, 0);
		loaded_configs.put(partitionName, config_inf) ;
		static_config = true;
		return;
	}

	// this method is not safe for many-Configuration&Partition-in-one-process case, must not be used
    // 2015.06.23 GLM: so, what shall we use instead??
	// return values: {"enabled", "disabled", "notfound"}
	public static synchronized ComponentStatus isComponentEnabled(String partition_name, String class_name, String object_id)
	{
		if ( object_id == null )
		{
			log.error("Trying to get isComponentEnabled for NULL object! Check your ESPER code.") ;
			return new ComponentStatus("notfound") ;
		}

		// Host IDs coming from sysadmin & netadmin tools are short ones
		if (class_name.equals("Computer") && !object_id.contains(".") )	object_id += ".cern.ch" ;

		config.ConfigObject cobj ;
		Configuration config ;

		// for PMG_Agent objects, need to check the state of corresponding host, by extracting its name
		if ( object_id.startsWith("PMG-AGENT_") )
		{
			log.trace("Looking for Computer instead of PMG-AGENT") ;
			class_name = "Computer" ;
			object_id = object_id.replaceFirst("PMG-AGENT_","") ;
		}

		log.debug("Trying to get enabled status for " + class_name + "@" + object_id) ;
		try 	{	
			config = getConfiguration(partition_name) ;
			if ( config == null )
			{
				log.info("Problem reading partition configuration") ;
				return new ComponentStatus("notfound")  ; // I do not know about configuration, may be RDB not yet loaded what to return?
			}
			cobj = config.get_object(class_name, object_id) ;
			if ( config.try_cast(class_name, "Component") )
			{
				log.trace("Casted to Component") ;
				dal.Component component = dal.Component_Helper.get(config, cobj) ;
				dal.Partition partition = dal.Partition_Helper.get(config, partition_name) ;
				return ( component.disabled(partition) ? new ComponentStatus("disabled") : new ComponentStatus("enabled") ) ;
			}
			if ( config.try_cast(class_name, "HW_Object") )
			{
				log.trace("Casted to HW_Object") ;
				dal.HW_Object component = dal.HW_Object_Helper.get(config, cobj) ;
				return ( component.get_State() ? new ComponentStatus("enabled") : new ComponentStatus("disabled") ) ;
			}
			log.error("Component " + class_name + "@" + object_id + " can not be disabled! Check your rule.") ;
			return new ComponentStatus("notfound") ; // if I can not cast, then it's fine
		}
		catch 	( NotFoundException ex )
		{
			log.debug("Object " + class_name + "@" + object_id + " not found: " + ex );
			return new ComponentStatus("notfound") ; // 
		}
		catch 	( Exception ex ) // extra safety
		{
			log.info("Problem reading partition configuration " + ex ) ;
			return new ComponentStatus("notfound") ; // I do not know about configuration, may be RDB not yet loaded what to return?
		}

	}

	private static java.util.Set<dal.Segment> getEnabledSegments(dal.Segment seg) throws config.ConfigException {
        java.util.Set<dal.Segment> segs = new HashSet<dal.Segment>();
		log.trace("Navigate into segments of" + seg.UID());
		dal.Segment[] segArray = seg.get_nested_segments();
		for (int i = 0; i < segArray.length; ++i) {
			if (segArray[i].is_disabled() == false) {
				segs.add(segArray[i]);	
				segs.addAll(getEnabledSegments(segArray[i]));
			}
		}
		return segs;
	}
	// GLM: Correct way to get status of a Segment
	public static synchronized BooleanBean isSegmentEnabled(String partition_name, String segment_id)
	{
		if ( segment_id == null )
		{
			log.error("Trying to get isComponentEnabled for NULL object! Check your ESPER code.") ;
			return new BooleanBean(false) ;
		}

		log.debug("Trying to get enabled status for Segment " + segment_id) ;
		try 	{	
			Configuration config = getConfiguration(partition_name) ;
			if ( config == null )
			{
				log.debug("No Configuration object, partition is not UP") ;
				return new BooleanBean(false) ; // I do not know about configuration, may be RDB not yet loaded what to return?
			}
			log.debug("Valid Config found");
			
			dal.Partition partition = dal.Partition_Helper.get(config, partition_name) ;
			if (partition == null) {
				log.error("No partition with name " + partition_name + " found.") ;
				return new BooleanBean(false) ;
			}
			log.debug("Valid Partition " + partition.UID() + " found");
			
			final dal.OnlineSegment os = partition.get_OnlineInfrastructure();
			if (os == null) {
				log.error("Online segment does not exist in partition " + partition_name) ;
				return new BooleanBean(false) ;
			}
			log.debug("Valid online segment found " + os.UID());

			final dal.Segment onlSeg = partition.get_segment(os.UID());
			if(onlSeg == null) {
				log.error("Online segment was found in partition " + partition_name + " but Segment could not be constructed.") ;				
				return new BooleanBean(false) ;				
			}

			log.debug("Valid online segment found");
			
           		if(onlSeg.is_disabled() == true) {
				log.error("Online segment is disabled in partition " + partition_name) ;
				return new BooleanBean(false) ;
            		}
            		if (segment_id.equals(os.UID())) {
            			return new BooleanBean(true) ;
            		}
            
			log.debug("Now call getEnabledSegments");

			java.util.Set<dal.Segment> segs = getEnabledSegments(onlSeg);
            		for(final dal.Segment seg : segs) {
            			if(seg.UID().equals(segment_id)) {
            			log.debug(segment_id + " is enabled in partition " + partition_name);
            			return new BooleanBean(true) ;
            			}
            		}
		}
		catch 	( config.NotFoundException ex )
		{
			log.debug("Segment " + segment_id + " not found: ", ex ) ;
			return new BooleanBean(false) ; // 
		}
		catch 	( SystemException ex )
		{
			log.info("Problem reading partition configuration ", ex ) ;
			return new BooleanBean(true) ; // better miss an alert but not have false-positive
		}
		catch 	( Exception ex ) // extra safety, have seen NullPointerException from line 314...
		{
			log.info("Failed to get enabled Segments, partition: " + partition_name + ". Cause: ", ex ) ;
			return new BooleanBean(true) ; 
			// some issue, we can not say if segment it enabled, but better not to have false-positive alerts
		}
		log.debug("Segment " + segment_id + " is not found in list of enabled segments in partition " + partition_name) ;
		return new BooleanBean(false) ;
	}
	
	public static synchronized IntegerBeam getNumForks(String partition_name) 
	{
		Configuration config ;
		String[] app_types = new String[]{"HLTRCApplication"};
		String[] segment_names = null;
		dal.Computer[] hosts = null;
		int count = 0;

		try {
			config = getConfiguration(partition_name);
			if (config == null) {
				log.debug("Configuration for " + partition_name + " cannot be read.") ;
				return new IntegerBeam(0);
				
			}
			dal.Partition partition = dal.Partition_Helper.get(config, partition_name);
			dal.BaseApplication[] apps = partition.get_all_applications(app_types, segment_names, hosts);
			
			for (dal.BaseApplication cc : apps) {
				HLTPUDal.HLTRCApplication rcapp = HLTPUDal.HLTRCApplication_Helper.cast(cc);
				count += rcapp.get_numForks() * rcapp.get_numberOfEventSlots();

			}

		} catch ( config.NotFoundException ex ) {
			log.error("Object not found: ", ex ) ;
		} catch ( Exception ex ) {
			log.error("Problem reading partition configuration ", ex ) ;
		}

		if (count == 0) {
			log.info("There are 0 PUs in partition " + partition_name);
		}
		return new IntegerBeam(count);
	}

	public static synchronized BooleanBean isPartitionOk (String partition_name) {
		if ( partition_name == null )
		{
			log.error("Trying to get isPartitionOk for NULL object! Check your ESPER code.") ;
			return new BooleanBean(false) ;
		}

		log.debug("isPartitionOk called for " + partition_name) ;
		try 	{	
			Configuration config = getConfiguration(partition_name) ;
			if ( config == null )
			{
				log.debug("No Configuration object, partition is not UP") ;
				return new BooleanBean(true) ; // I do not know about configuration, may be RDB not yet loaded what to return?
			}
			log.debug("Valid Config found");
			
			dal.Partition partition = dal.Partition_Helper.get(config, partition_name) ;
			
			log.debug("Valid Partition " + partition.UID() + " found");

			String[] app_types = new String[]{"HLTSVApplication", "DcmApplication"};
			String[] segment_names = null;
			dal.Computer[] hosts = null;

			dal.BaseApplication[] apps = partition.get_all_applications(app_types, segment_names, hosts);
			
			if (apps.length > 1) {
				System.err.println("there are " + apps.length  + " apps of the good type");
				return new BooleanBean(true);					
			}
			else {
				log.error("Not found HLTSVApplication and DcmApplication in the list of ATLAS applications: " + apps);
				return new BooleanBean(false) ;
			}

		} catch ( config.NotFoundException ex ) {
			log.error("Object not found: ", ex ) ;
		} catch ( Exception ex ) {
			log.error("Problem reading partition configuration ", ex ) ;
		} 
		log.error("Something wrong happened in isPartitionOk, can not get result") ;
		return new BooleanBean(true) ;
	}
	
	
	public static ArrayList<String> connectedHost(String cd1Name)
	{
		// JDBC driver name and database URL
		String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		String DB_URL = "jdbc:mysql://pc-tdq-net-mon-es/es"; 

		// Database credentials
		String USER = "ronly";
		String PASS = "coffe";

		Connection conn = null;
		Statement stmt = null;
		ArrayList<String> connectivityHost = new ArrayList<String>(); 

		try{
			// Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			// OPen a connection
			conn = DriverManager.getConnection(DB_URL,USER,PASS);


			// Execute a query
			stmt = conn.createStatement();
			String sql = null;
			sql = "SELECT peer_device FROM core_device AS cd1 INNER JOIN core_interface AS ci1 ON cd1.id = ci1.device_id WHERE cd1.name = '" + cd1Name + "' and ci1.peer_device like 'pc%'";

			ResultSet rs = stmt.executeQuery(sql);

			// Extract data from result set
			while(rs.next()){
				String name = rs.getString("peer_device"); 
				connectivityHost.add(name);
			}

			// Clean up environment
			rs.close();
			stmt.close();
			conn.close();
		}catch(SQLException se){
			// Handle errors for JDBC
			log.error("errors happen while importing JDBC"); 
		}catch(Exception e){
			// Handle errors for class.forName
			log.error("can not find JDBC driver");
		}finally{
			// close resources
			try{
				if(stmt != null)
					stmt.close();
			}catch(SQLException se2){
			} // nothing we can do
			try{
				if(conn != null)
					conn.close();
			}catch(SQLException se){
				log.error("error accessing database");
			}

		}

		return connectivityHost;
	}

	// to find out if a network device is in use or not
	public static BooleanBean isDeviceInUse(String partition_name, String device_name) 
	{
		if(device_name == null) {
			log.error("No device passed to isDeviceInUse. Check your ESPER code");
			return new BooleanBean(true);
		}

		ArrayList<String> connectivityHost = new ArrayList<String>();
		if(device_name.contains("sw")){						      // device is a switch
			if(device_name.contains("edge") || device_name.contains("core"))      //  core or edge switch
				return new BooleanBean(true);                                 // always true
			connectivityHost = connectedHost(device_name);
		}

		String[] app_types = null;
		String[] segment_names = null;
		dal.Computer[] hosts = null;

		try {
			Configuration config = getConfiguration("ATLAS");

			if(config == null) { // e.g. partition is not running
				log.debug("No Configuration object, partition is not UP");
				return new BooleanBean(false); // we don't know
			}

			dal.Partition partition = dal.Partition_Helper.get(config, partition_name);
			dal.BaseApplication[] apps = partition.get_all_applications(app_types, segment_names, hosts);

			for(dal.BaseApplication cc : apps) {
				String hname = cc.get_host().UID().toString();
				// device is a pc.* 
				if(hname.contains(device_name))
					return new BooleanBean(true);				

				// check all connected hosts
				for(String ch : connectivityHost) {
					if(hname.contains(ch))
						return new BooleanBean(true);
				}
			}			

		} catch (config.NotFoundException ex) {
			log.error("Segment not found: ", ex);
		} catch (java.lang.NullPointerException ex) {
			log.error("Unexpected: ", ex);
		} catch (Exception ex) {
			log.error("Problem reading partition configuration ", ex);
		}
		return new BooleanBean(false);
	}

        // to find out if a peer is in use or not
        public static BooleanBean isPeerInUse(String partition_name, String peer_name)
        {
                if(peer_name == null) {
                        log.error("No peer passed to isPeerInUse. Check your ESPER code");
                        return new BooleanBean(false);
                }

                if(peer_name.contains("sw")) {
                        if(peer_name.contains("tpu"))
                                return isTPUSwitchInUse(partition_name, peer_name);
		        return new BooleanBean(true);
		}

                String[] app_types = null;
                String[] segment_names = null;
                dal.Computer[] hosts = null;

                try {
                        Configuration config = getConfiguration("ATLAS");

                        if(config == null) { // e.g. partition is not running
                                log.debug("No Configuration object, partition is not UP");
                                return new BooleanBean(false); // we don't know
                        }

                        dal.Partition partition = dal.Partition_Helper.get(config, partition_name);
                        dal.BaseApplication[] apps = partition.get_all_applications(app_types, segment_names, hosts);

                        for(dal.BaseApplication cc : apps) {
                                String hname = cc.get_host().UID().toString();

                                // check connected peer
                                if(hname.contains(peer_name))
                                        return new BooleanBean(true);
                        }

                } catch (config.NotFoundException ex) {
                        log.error("Segment not found: ", ex);
                } catch (java.lang.NullPointerException ex) {
                        log.error("Unexpected: ", ex);
                } catch (Exception ex) {
                        log.error("Problem reading partition configuration ", ex);
                }
                return new BooleanBean(false);
        }

        // to find out if a TPU switch is in use or not
        public static BooleanBean isTPUSwitchInUse(String partition_name, String switch_name)
        {
                if(switch_name == null) {
                        log.error("No device passed to isTPUSwitchInUse. Check your ESPER code");
                        return new BooleanBean(false);                 // This should not happen.
                }

                String[] app_types = null;
                String[] segment_names = null;
                dal.Computer[] hosts = null;

                try {
                        Configuration config = getConfiguration("ATLAS");

                        if(config == null) { // e.g. partition is not running
                                log.debug("No Configuration object, partition is not UP") ;
                                return new BooleanBean(false); // we don't know
                        }

                        String tpu_name = "pc-tdq-tpu-" + switch_name.substring(switch_name.lastIndexOf('-') + 1);

                        dal.Partition partition = dal.Partition_Helper.get(config, partition_name);
                        dal.BaseApplication[] apps = partition.get_all_applications(app_types, segment_names, hosts);

                        for(dal.BaseApplication cc : apps) {
                                String hname = cc.get_host().UID().toString();

                                if(hname.contains(tpu_name))
                                        return new BooleanBean(true);
                        }

                 } catch (config.NotFoundException ex) {
                         log.error("Segment not found: ", ex);
                 } catch (java.lang.NullPointerException ex) {
                         log.error("Unexpected: ", ex);
                 } catch (Exception ex) {
                         log.error("Problem reading partition configuration ", ex);
                 }
                 return new BooleanBean(false);
         }
}
