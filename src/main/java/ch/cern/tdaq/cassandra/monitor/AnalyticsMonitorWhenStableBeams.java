package ch.cern.tdaq.cassandra.monitor;

import com.espertech.esper.client.EPAdministrator;
import com.espertech.esper.client.EPStatement;

public class AnalyticsMonitorWhenStableBeams {
	
	public static EPStatement getStatement(EPAdministrator cepAdm) {
		return cepAdm.createEPL("select  applicationName, sum(cast(info.value, int)) " +
				"from method:ISReader.getISInfo(\"test_mrs\", \".RunParams\", 0) " +
				"as info, MRSEvent.win:time_batch(5 sec) group by applicationName");
	}
	
	

}
