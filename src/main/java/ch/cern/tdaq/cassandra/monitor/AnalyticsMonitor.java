package ch.cern.tdaq.cassandra.monitor;

import com.espertech.esper.client.EPAdministrator;
import com.espertech.esper.client.EPStatement;

public class AnalyticsMonitor {
	
	public static EPStatement getStatement(EPAdministrator cepAdm) {
		//return cepAdm.createEPL("select applicationName, messageID, sum(numberOfMessages), messageTxt, machineName from " +
		//		"MRSEvent.win:time_batch(5 sec)  " +
		//"group by applicationName");
		
		return cepAdm.createEPL("select applicationName, machineName, messageTxt, sum(numberOfMessages) from " +
				"MRSEvent.win:time_batch(2 sec)  " +
		"group by applicationName, machineName, messageTxt");
	}

}
