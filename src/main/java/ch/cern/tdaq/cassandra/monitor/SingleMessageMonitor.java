package ch.cern.tdaq.cassandra.monitor;

import com.espertech.esper.client.EPAdministrator;
import com.espertech.esper.client.EPStatement;

public class SingleMessageMonitor {
	
	public static EPStatement getStatement(EPAdministrator cepAdm) {
		//return cepAdm.createEPL("select applicationName, messageID, sum(numberOfMessages), messageTxt, machineName from " +
		//		"MRSEvent.win:time_batch(5 sec)  " +
		//"group by applicationName");
		
		return cepAdm.createEPL("select * from " +
				"MRSEvent.win:time_batch(2 sec) where  applicationName='APPNAME_0' ");
	}

}
