package ch.cern.tdaq.cassandra.event;

public class ConfigurationEvent {
	
	private  String name; //MDT-ATDS...
	
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public ConfigurationEvent(String name) {
		super();
		this.name = name;
	}
}
