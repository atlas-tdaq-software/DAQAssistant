package ch.cern.tdaq.cassandra.event;

public class InvalidEventException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 /**
     * Ctor.
     * @param message - error message
     */
    public InvalidEventException(final String message, Exception cause) {
        super(message, cause);
    }

}
