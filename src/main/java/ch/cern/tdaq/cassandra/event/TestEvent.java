package ch.cern.tdaq.cassandra.event;


public class TestEvent {

    private int number;

    public TestEvent(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

}
