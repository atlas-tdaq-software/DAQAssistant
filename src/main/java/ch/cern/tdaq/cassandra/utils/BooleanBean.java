package ch.cern.tdaq.cassandra.utils;

public class BooleanBean {
		
		private boolean bool_value;
		
		public BooleanBean(boolean b) {
			bool_value = b ;
		}
				
		public boolean getBoolean() {
			return bool_value ;
		}

}
