package ch.cern.tdaq.cassandra.utils;

import java.net.HttpURLConnection ;
import java.net.URL ;
import java.net.MalformedURLException ;
import java.io.BufferedReader ;
import java.io.InputStreamReader ;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.JsonElement;


// uses gitea web API to query gitea for the presence of open pull requests with sertain label (e.g. "SA")

public class GiteaAPI {
    
    // test example: $TDAQ_JAVA_HOME/bin/java -cp $TDAQ_CLASSPATH ch.cern.tdaq.cassandra.utils.GiteaAPI "tdaq-09-04-00" "ATLAS"

    public static void main(String [] args) {
        StringArrayBean result = getOpenPulls(args[0], args[1]) ;
        String[] arr = result.getArray() ;
        for (int i = 0; i < arr.length; i++) {
            System.err.println(arr[i]) ;
        }
    }

    private static final Log log = LogFactory.getLog(GiteaAPI.class);
    
    // P1: http://pc-tdq-git.cern.ch/gitea/api/v1/repos/oks/tdaq-09-02-01/pulls 
    final static String GITEA_URL = "http://pc-tdq-git.cern.ch/gitea/api/v1/repos/oks/" ;

    public static StringArrayBean getOpenPulls(final String repo, final String label) {
        try {
            URL url = new URL(GITEA_URL + repo + "/pulls") ; 
            HttpURLConnection connection = (HttpURLConnection) url.openConnection() ;
            connection.setRequestMethod("GET") ;
            connection.setRequestProperty("accept", "application/json") ;
            connection.setConnectTimeout(5000) ;
            connection.setReadTimeout(5000) ;
            
            int status = connection.getResponseCode() ; // here the request is executed
            log.debug("URL request status: " + status) ;
            
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine ;
            StringBuffer content = new StringBuffer();
            while ( (inputLine = in.readLine()) != null ) {
                  content.append(inputLine);
              }
            in.close() ;
            connection.disconnect() ;

            final java.util.List<String> ret = new java.util.ArrayList<String>() ;

            JsonArray pulls = new JsonParser().parse(content.toString()).getAsJsonArray() ;
            for (int i = 0; i < pulls.size(); i++) {
                String state = pulls.get(i).getAsJsonObject().get("state").getAsString() ;
                if ( state.equals("open") ) {
                    try {
		    	JsonArray jalabels = pulls.get(i).getAsJsonObject().getAsJsonArray("labels") ;
		    	for ( int j=0; j<jalabels.size(); j++ ) {
				String lbl = jalabels.get(j).getAsJsonObject().get("name").getAsString() ;
				if ( lbl.equals(label) ) {
			        	String title = pulls.get(i).getAsJsonObject().get("title").getAsString() ;
                        		String request_url = pulls.get(i).getAsJsonObject().get("url").getAsString() ;
                        		String author = pulls.get(i).getAsJsonObject().get("user").getAsJsonObject().get("full_name").getAsString() ;
                        		String assignee = "" ;
                        		JsonElement assign = pulls.get(i).getAsJsonObject().get("assignee") ;
                        		if ( assign != null && assign.isJsonObject() ) {
                            			assignee = assign.getAsJsonObject().get("full_name").getAsString() ;
                        		} 
                        		String date = pulls.get(i).getAsJsonObject().get("created_at").getAsString() ;
                        		String comment = pulls.get(i).getAsJsonObject().get("body").getAsString() ;

                        		ret.add("Merge request: " + title + "\nDate: " + date + "\nAuthor: " + author + 
                                	"\nURL: " + request_url + "\nAssignee: " + assignee + "\nComment: "  + comment + "\n\n" ) ;
					log.info("Selected MR \"" + title + "\" with label: " + lbl) ;
					break ;
				}
				else {
					log.info("Skipped MR with label: " + lbl) ;
				}
			}
		                        }
                    catch ( final IllegalStateException ex ) {
                        ex.printStackTrace() ;
                        continue ;
                    }
                    catch ( final Exception ex ) { // wrong JSON schema...
                        ex.printStackTrace() ;
                    }
                }
            }
            
            return new StringArrayBean(ret.toArray(new String[ret.size()])) ;

        } catch ( final MalformedURLException ex ) {
            ex.printStackTrace();
        } catch ( final java.io.IOException ex ) {
            ex.printStackTrace();
        }

        return new StringArrayBean(new String[0]) ;   
    }

    // return vector of open pull reuqest, with selected fileds, in JSON format like
    // {"request":"test branch","author":"Andrei Kazarov","state":"open","comment":"Hello, please accept my **nice** mods in the readme file!\r\n*Andrei*","assigne":"Andrei Kazarov","URL":"http://pc-tdq-git.cern.ch/gitea/oks/tdaq-09-02-01/pulls/3"}
    // i.e. one line (esper Event) per pull
    // it is assumed that gitea_query script is in PATH
    public static StringArrayBean getOpenPullsShell(final String repo) {
        try {
            final ProcessBuilder pb = new ProcessBuilder("gitea_query.sh", repo);
            pb.redirectErrorStream(true);
            final java.util.List<String> ret = new java.util.ArrayList<String>() ;
            final Process last_command = pb.start() ;
            final BufferedReader br = new BufferedReader(new InputStreamReader(last_command.getInputStream()), 16);
            String line ;
            while ((line = br.readLine()) != null) {
                log.debug("Got output from gitea_query: " + line);
                ret.add(line) ;
            }
            int status = last_command.waitFor() ;
            if ( status == 0 ) {
                return new StringArrayBean(ret.toArray(new String[ret.size()])) ;
            }
            log.debug("Process gitea_query.sh finished with status " + status);  
        } catch ( final Exception ex ) {
            log.error("Failed to execute gitea_query.sh: " + ex);
            // ex.printStackTrace() ;
        }
        return new StringArrayBean(new String[0]) ;       
    }
}
