package ch.cern.tdaq.cassandra.utils;

public class ComponentStatus {
		private String status_value;
		
		public ComponentStatus(String s) {
			status_value = s ;
		}
				
		public String getStatus() {
			return status_value ;
		}

}
