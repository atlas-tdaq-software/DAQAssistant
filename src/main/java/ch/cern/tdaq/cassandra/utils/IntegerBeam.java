package ch.cern.tdaq.cassandra.utils;

public class IntegerBeam {
	private final Integer v;
	
	public IntegerBeam(Integer i) {
		v = i;
	}
	
	public Integer getValue(){
		return v;
	}
}
