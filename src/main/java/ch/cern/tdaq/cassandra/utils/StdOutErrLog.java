package ch.cern.tdaq.cassandra.utils;

import java.io.PrintStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class StdOutErrLog {

	private static final Log log = LogFactory.getLog(StdOutErrLog.class.getName());

	public static void tieSystemOutAndErrToLog() {
		System.setOut(createLoggingProxy(System.out));
		System.setErr(createLoggingProxy(System.err));
	}

	public static PrintStream createLoggingProxy(final PrintStream realPrintStream) {
		return new PrintStream(realPrintStream) {
			public void print(final String string) {
				realPrintStream.print(string);
				log.info(string);
			}
		};
	}
}
