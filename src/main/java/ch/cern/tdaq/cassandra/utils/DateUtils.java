package ch.cern.tdaq.cassandra.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DateUtils {
	private static DateFormat df = new SimpleDateFormat("dd MMM yyyy HH:mm:ss z");
	
	public static String format(Long epoch_time) {
		return df.format(new Date(epoch_time));
	}

}
