package ch.cern.tdaq.cassandra.utils;

public class ArrayBean {
		
		private Integer[] array;
		
		public ArrayBean(Integer[] a) {
			array =a;
		}
		
		public Integer[] getArray() {
			return array;
		}
		
		public  Integer getSize() {
			return array.length;
		}

}
