package ch.cern.tdaq.cassandra.utils;

public class StringArrayBean {
		
		private String[] array;
		
		public StringArrayBean(String[] a) {
			array =a;
		}
		
		public String[] getArray() {
			return array;
		}
		
		public Integer getSize() {
			return array.length;
		}

}