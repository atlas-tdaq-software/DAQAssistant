package ch.cern.tdaq.cassandra.utils;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


// to decode SCT published busy and status information encoded in int32 bits
// SCTAPIServer.MON_TIM_REG_RB_STAT_P0_Cx

public class SCTUtils {
	
	private static final Log log = LogFactory.getLog(SCTUtils.class);
		// bit masks of a nominal configuration
	final static int bitmask =    Integer.parseInt("1111111111111111", 2) ; 


	// retrurn busy status by module position
	// SCTAPIServer.MON_TIM_REG_RB_STAT_P0_Cx
	public static boolean isModuleBusy(int bits, int slot)
	{
	    if ( bits == 0 ) return false ;
	    final int bit = slot - (slot<13 ? 5 : 6);
	    return (bits & 1<<bit) != 0;
	}

	// returns boolean indicating enabled/disabled status of this module (as busy mask)
	// SCTAPIServer.MON_TIM_REG_RB_MASK_P0_Cx	
	public static boolean isModuleEnabled(int bits, int slot, int crate)
	{	
		int bit ; 
		if ( slot < 13 ) bit = slot - 5 ;
		else bit = slot - 6 ;

		// some slots are not counted, no modules installed
		if ( bit == 0 || bit == 4 || bit == 11 || bit == 15 ) return true ;
		if ( !(crate == 4 || crate == 7) && bit == 14 ) return true ;

		if ( (bits & (1<<bit)) != 0 ) { return true ; }

		return false ;
	}

	// returns array of disabled modules 
	// SCTAPIServer.MON_TIM_REG_RB_MASK_P0_Cx	
	public static int[] getDisabledModules(int bits, int crate)
	{	
		int disabled_bits = 0 ;

		disabled_bits = bitmask - bits ;

		int[] ret = new int[Integer.bitCount(disabled_bits)] ;

		int ix = 0 ;
		for ( int i=0;  i<8 ; i++ ) if ( (disabled_bits & (1<<i)) != 0 ) { ret[ix++] = i+5 ; }
		for ( int i=8; i<16 ; i++ ) if ( (disabled_bits & (1<<i)) != 0 ) { ret[ix++] = i+6 ; }

		return ret ;
	}

	// returns array of disabled modules 
	// SCTAPIServer.MON_TIM_REG_RB_MASK_P0_Cx	
	public static ArrayBean getDisabledModulesBean(int bits, int crate)
	{	
		int disabled_bits = 0 ;

		disabled_bits = bitmask - bits ;

		log.trace("disabled_bits: " + disabled_bits);

		Integer[] ret = new Integer[Integer.bitCount(disabled_bits)] ;

		log.trace("array lendth: " + Integer.bitCount(disabled_bits)) ;		

		int ix = 0 ;
		for ( int i=0;  i<8 ; i++ ) if ( (disabled_bits & (1<<i)) != 0 ) { ret[ix++] = i+5 ; }
		for ( int i=8; i<16 ; i++ ) if ( (disabled_bits & (1<<i)) != 0 ) { ret[ix++] = i+6 ; }

		return new ArrayBean(ret) ;
	
	}

	// returns array of indexes of positions of busy modules
	// assumes that bits !=0 
	// SCTAPIServer.MON_TIM_REG_RB_STAT_P0_Cx
	public static int[] getBusyModules(int bits)
	{
		//System.out.println("Bits: " + Integer.bitCount(bits)) ;
		int[] ret = new int[Integer.bitCount(bits)] ;

		int ix = 0 ;
		for ( int i=0;  i<8 ; i++ ) if ( (bits & (1<<i)) != 0 ) { ret[ix++] = i+5 ; }
		for ( int i=8; i<16 ; i++ ) if ( (bits & (1<<i)) != 0 ) { ret[ix++] = i+6 ; }

		return ret ;
	}

	// returns array of indexes of positions of busy modules
	// assumes that bits !=0 
	// SCTAPIServer.MON_TIM_REG_RB_STAT_P0_Cx
	public static ArrayBean getBusyModulesBean(int bits)
	{
		//System.out.println("Bits: " + Integer.bitCount(bits)) ;
		Integer[] ret = new Integer[Integer.bitCount(bits)] ;

		int ix = 0 ;
		for ( int i=0;  i<8 ; i++ ) if ( (bits & (1<<i)) != 0 ) { ret[ix++] = i+5 ; }
		for ( int i=8; i<16 ; i++ ) if ( (bits & (1<<i)) != 0 ) { ret[ix++] = i+6 ; }
		
		return new ArrayBean(ret) ;
	}

	public static void main(String [] args) throws InterruptedException{

		//int test1 = 0b100100000001001 ;
		int test1x = 0x4809 ;
		//int test2 = 0b100000000000000 ;
		int test2x = 0x4000 ;
		///int test3 = 0b000000000000010 ;
		int test3x = 0x0002 ;

		int hexVal = 0x1a;

		getDisabledModules(1,1) ;

		return ;

		/*
		System.out.println(test1x) ;
		System.out.println(" isModuleBusy(" + test1x + ",14) = " + isModuleBusy(test1x,14)) ;
		System.out.println(" isModuleBusy(" + test1x + ",5)  = " + isModuleBusy(test1x,5)) ;
		System.out.println(" getBusyModules(" + test1x + ") = " + getBusyModules(test1x)[0]) ;
		System.out.println(" getBusyModules(" + test1x + ") = " + getBusyModules(test1x)[1]) ;
		System.out.println(" getBusyModules(" + test1x + ") = " + getBusyModules(test1x)[2]) ;
		System.out.println(" getBusyModules(" + test2x + ") = " + getBusyModules(test2x)[0]) ;
		System.out.println(" getBusyModules(" + test3x + ") = " + getBusyModules(test3x)[0]) ;
		 */	}




}


