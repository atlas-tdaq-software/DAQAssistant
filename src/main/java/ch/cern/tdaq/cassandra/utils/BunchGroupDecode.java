package ch.cern.tdaq.cassandra.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class BunchGroupDecode {
	private static final Log log = LogFactory.getLog(BunchGroupDecode.class);
	
	/**
	 * This function take in input an array dump of the hystogram 
	 * representing the comparison between the expected Bunch Group settings
	 * and the ones defined by LHC.
	 * If any entry is different then 0, the function evaluate the
	 * bunch group corresponding to the wrong entry
	 *  (the mapping entry-bunch group is defined by Joerg mail)
	 *  
	 *  If everything is ok (all entries = 0), -1 is returned
	 *  
	 * @param hystogram : the array dump of the hystogram
	 * @return index of the corresponding bunch group
	 */
	public static IntegerBeam getWrongBunchGroup(Float[] hystogram) {
		if ( hystogram != null ) // protection from no-data condition
		for (int i=0; i<hystogram.length;i++) {
			if (hystogram[i]!=0){
				return new IntegerBeam(i%8); // In case the array dump is Y * X 
			}
		}

		//Everything was fine
		return new IntegerBeam(-1);
	}

}
