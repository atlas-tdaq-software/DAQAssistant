package ch.cern.tdaq.cassandra;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.LogConfigurationException ;

/**
 * This class contains the <code>main</code> to start the AAL engine.
 * 
 * @param args Arguments
 */
public class AALMain {
	
	private static Log log = null ;

	static {
		try {
		log = LogFactory.getLog(AALMain.class) ;
		}
		catch ( LogConfigurationException ex )
			{
			System.exit(1) ;
			}
	}

	public static void main(String [] args) {
		
		try {
		log.info("Configuring AAL engine...");
		AALEngine.getInstance().configure();
		log.info("Configure stage done, starting and waiting for kill signal");
		AALEngine.getInstance().start(); // will block untill System.exit()
		log.info("Engine finished");
	}
	catch (final RuntimeException ex)
		{
			log.error("Failed to run AAL engine: " + ex) ;
			ex.printStackTrace();
			System.exit(1) ;
		}

	log.info("Exiting AAL. Good bye!");

	}
}
